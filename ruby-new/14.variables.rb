def basic_method
  # x = 10
  puts $x
end

$x = 10
basic_method

class Car
  def initialize(brand:)
    @brand = brand

    if defined?(@@number_of_cars)
      @@number_of_cars += 1
    else
      @@number_of_cars = 1
    end
  end

  def start
    puts "#{@brand} Starting"
  end


  def self.count
    @@number_of_cars
  end
end

vw = Car.new(brand: 'Volkswagen')
mr = Car.new(brand: 'Mercedez')
# vw.start
puts Car.count
