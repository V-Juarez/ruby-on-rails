[1, 3, 4].each { |i| puts i}


[1, 2, 3, 4].each do |i|
  if i % 2 == 0
    puts "#{i} is even"
  else
    puts "#{i} is odd"
  end
end

class PDFReporter
  def generate(title, month)
    puts "generando reporte #{title}"
    # puts 'generando reporte'
    if block_given?
      yield month
    end
  end
end

reporte = PDFReporter.new
reporte.generate('Mensual', 'Octubre') do |month|
  puts "Reporte de #{month}"
end



# { puts "Reporte del dia"}
