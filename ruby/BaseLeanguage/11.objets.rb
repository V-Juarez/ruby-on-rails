class Persona

  # metodos de clase
  def self.suggested_names
    ["Pepe", "Pepito", "Saturano"]
  end

  # Constructor
  def initialize(name, age)
    @name = name
    @age = age
  end

  # methods
  def name
    # Instancias
    @name
  end


  # geter
  def age
    @age
  end

  # seter
  # persona.name = "pepe"
  # persona.name = "pepe".age = 7
  def name=(name)
    @name = name
    self
  end

  def age=(age)
    @age = age
    self
  end
end

persona = Persona.new('pepe', 14)
puts persona.name
puts persona.age