<h1>Ruby on Rails</h1>

<h3>Johan Tiqye</h3>

<h2>Table of Content</h2>

- [1. Introducción](#1-introducción)
  - [Todo lo que aprenderás sobre Ruby on Rails](#todo-lo-que-aprenderás-sobre-ruby-on-rails)
  - [¿Qué es Ruby on Rails y por qué usarlo?](#qué-es-ruby-on-rails-y-por-qué-usarlo)
- [2. Entorno de desarollo](#2-entorno-de-desarollo)
  - [Entorno de desarrollo de Ruby on Rails](#entorno-de-desarrollo-de-ruby-on-rails)
  - [Instalación de Ruby, RoR en Linux](#instalación-de-ruby-ror-en-linux)
  - [Instalación de Ruby, RoR en Mac y Windows](#instalación-de-ruby-ror-en-mac-y-windows)
- [3. Nuestra primera aplicación](#3-nuestra-primera-aplicación)
  - [Entender la web con rieles](#entender-la-web-con-rieles)
  - [Primero pasos con Ruby on Rails](#primero-pasos-con-ruby-on-rails)
  - [Entender el enrutamiento básico](#entender-el-enrutamiento-básico)
  - [Manipular el patrón MVC](#manipular-el-patrón-mvc)
  - [Los secretos de Rails](#los-secretos-de-rails)
  - [Assets y Layouts](#assets-y-layouts)
  - [Agregar el primer conjunto de scaffolds](#agregar-el-primer-conjunto-de-scaffolds)
  - [Cómo funcionan las migraciones](#cómo-funcionan-las-migraciones)
  - [Optimiza tu código con HAML](#optimiza-tu-código-con-haml)
  - [Agiliza la construcción de formularios con Simple Form](#agiliza-la-construcción-de-formularios-con-simple-form)
  - [Soporte de varios idiomas para tu aplicación](#soporte-de-varios-idiomas-para-tu-aplicación)
- [Debugging: detecta los errores en tu código](#debugging-detecta-los-errores-en-tu-código)
- [4. Proyecto del curso: primeros pasos](#4-proyecto-del-curso-primeros-pasos)
  - [¿Qué vamos a desarrollar?](#qué-vamos-a-desarrollar)
  - [Diseñando el modelo de datos](#diseñando-el-modelo-de-datos)
  - [Construye los primeros scaffolds del proyecto](#construye-los-primeros-scaffolds-del-proyecto)
  - [Internacionalizando los modelos](#internacionalizando-los-modelos)
  - [Agregando validaciones al modelo](#agregando-validaciones-al-modelo)
- [5. Proyecto del curso: usuarios](#5-proyecto-del-curso-usuarios)
  - [Añadiendo el concepto de usuario](#añadiendo-el-concepto-de-usuario)
  - [Asignando un propietario a la tarea](#asignando-un-propietario-a-la-tarea)
  - [Añadiendo participantes a la tarea](#añadiendo-participantes-a-la-tarea)
  - [Creando formularios anidados](#creando-formularios-anidados)
  - [Interactuando con Cocoon para anidar formularios](#interactuando-con-cocoon-para-anidar-formularios)
  - [CanCan: ¿puedes hacerlo?](#cancan-puedes-hacerlo)
- [6. Proyecto del curso: interacciones](#6-proyecto-del-curso-interacciones)
  - [Callbacks en Rails](#callbacks-en-rails)
  - [Añadiendo datos semilla](#añadiendo-datos-semilla)
  - [Enviando e-mails a los participantes](#enviando-e-mails-a-los-participantes)
  - [Añandiendo notas a la tarea](#añandiendo-notas-a-la-tarea)
  - [Añadiendo notas con AJAX](#añadiendo-notas-con-ajax)
- [7. Cierre](#7-cierre)
  - [Embelleciendo nuestra aplicación](#embelleciendo-nuestra-aplicación)
  - [Desplegando a Heroku](#desplegando-a-heroku)
  - [Conclusiones del curso](#conclusiones-del-curso)

# 1. Introducción

## Todo lo que aprenderás sobre Ruby on Rails

Envio de Emails

## ¿Qué es Ruby on Rails y por qué usarlo?

Ruby es un lenguaje de programación interpretado, reflexivo y orientado a objetos (todo es un objeto, incluso los números). Combina una sintaxis inspirada en Python y Perl con características de programación orientada a objetos similares a Smalltalk. Comparte también funcionalidad con otros lenguajes de programación como Lisp, Lua, Dylan y CLU. Además de todo esto es totalmente libre. No sólo gratis, sino también libre para usarlo, copiarlo, modificarlo y distribuirlo.

Algunas de sus características son:

- Es de tipado dinámico, osea las variables no están asociadas a un tipo de dato específico (entero, string, boleano, float, etc) permitiendonos realizar cambios en tiempo de ejecución.
- Es interpretado, una cualidad que nos permite mayor agilidad al momento de ejecutar nuestro programa pues ya no será necesario compilar el programa, así como tampoco necesitaremos binarios para cada uno de los sistemas operativos donde lo ejecutaremos, existen varios interpretes en Ruby CRuby, JRuby, Mruby, Rubinius, Opal, RubyMotion.
- DRY, No te repitas (en inglés Don’t Repeat Yourself, también conocido como Una vez y sólo una) según este principio toda pieza de información nunca debería ser duplicada debido a que la duplicación incrementa la dificultad en los cambios y evolución posterior, puede perjudicar la claridad y crear un espacio para posibles inconsistencias.
- La filosofía de Ruby es hacer la programación más humana, mejorar la productividad y felicidad del desarrollador, siguiendo los principios de una buena interfaz de usuario, sostiene que el diseño de sistemas necesita enfatizar las necesidades humanas más que las de la máquina, su objetivo es que cuando te sientas a leer el código sea más natural.
- Es un lenguaje que nos permite realizar metaprogramación - “Es la habilidad de usar código para generar código”
- También nos permite crear DSL o Lenguaje específico de dominio, que son herramientas increíblemente poderosas para facilitar la programación o configuración de sistemas complejos, o programas dedicados a resolver un problema en particular.
- Ruby posee una comunidad bastante activa y a sus paquetes o librerías se les conoce como “gemas” actualmente existen unas 161.000 gemas aprox, que se han descargado alrededor de 55 millones de veces.
- Esta muy bien diseñado al punto que a inspirado a otras tecnologías como elixir o CoffeeScript, D, Groovy, Falcon y Swift.
- El ecosistema ruby es muy completo, el lenguaje es utilizado para
  - WEB con frameworks como Ronu on Rails, Sinatra, Rack, Grape
  - Mobile, RubyMotion es una implementación del lenguaje Ruby para la creación de aplicaciones en iOS, OS X y Android.
  - Utilidades de terminal, lo que te imagines un ejemplo es HomeBrew
  - Aplicaciones gráficas (programas de escritorio) QT, SWING, Ruby GTK, Shoes, FxRuby, Zenity,VisualRuby, QtRuby, SharedDevelop


Creo que la mejor forma de aprender es enseñar, por si les interesa tenemos un server en Discord “Aprende Ruby On Rails” donde tocamos temas referentes a Ruby y Ruby on Rails, son bienvenidos a compartir sus experiencias, les dejo el link https://discord.gg/N84JsXW

Conceptos extraídos de:
[Ruby](https://www.ruby-lang.org/es/about/)
[Gemas](https://rubygems.org/)
[Tipado dinámico](https://charlascylon.com/2017-05-10-dynamic-vs-static)
[Lenguajes compilados e interpretados](https://blog.makeitreal.camp/lenguajes-compilados-e-interpretados/)
 - https://www.toptal.com/ruby/the-many-shades-of-the-ruby-programming-language
[Filosofía Ruby](https://www.informit.com/articles/article.aspx?p=18225)
 - https://www.artima.com/intv/ruby.html

[DRY](http://joaquin.medina.name/web2008/documentos/informatica/documentacion/logica/OOP/Principios/2012_07_30_OopNoTeRepitas.html)
[Metaprogramación](https://medium.com/@hackvan/entendiendo-la-metaprogramación-con-ruby-7a0360ee67e7)
[DSL](https://www.toptal.com/ruby/ruby-dsl-metaprogramming-guide)
[Ruby Influencia otros lenguajes](https://royalsocietypublishing.org/doi/10.1098/rsif.2015.0249)
 - https://es.slideshare.net/AlexanderEcheverra/lenguaje-de-programacin-ruby-69781302
Ecosistema ruby
  - https://www.jetbrains.com/lp/devecosystem-2020/
  - https://www.jetbrains.com/es-es/lp/evecosystem-2020/ruby/

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - terranigmark/curso-introduccion-ruby-on-rails: Repositorio utilizando para las clases del Curso de Introducción de Ruby on Rails en Platzi (https://platzi.com/cursos/intro-ror/)https://github.com/terranigmark/curso-introduccion-ruby-on-rails](https://github.com/terranigmark/curso-introduccion-ruby-on-rails)

# 2. Entorno de desarollo

## Entorno de desarrollo de Ruby on Rails

Rails competences

![Rails competences](https://dgosxlrnzhofi.cloudfront.net/custom_page_images/production/64/page_images/Rails_Competencies.png?1386276348)

Ruby on Rails está compuesto de una serie de módulos esenciales que resuelven problemas específicos del desarrollo de software web.

- **ActiveRecord →** Este te provee una estructura para poder hacer la interconexión con la DB a través del concepto de modelo. Pero importante saber, que desde el modelo vas a hacer consultas a tus DB y traer y manipular información.
- **ActionView** → Traeremos la información de la DB la renderizaremos y la mostraremos con ayuda de este módulo.
- **ActionMailer** → Para enviar Emails utilizaremos este módulo.
- **Rails + JS →** Aquí uniremos el mundo de Rails con el JS.

RoR está compuesto de una serie de módulos, cada uno orientado a resolver un problema de desarrollo web.

#### Módulos esenciales

- ActiveRecord: Provee una estructura para crear una conexión a una base de datos a través del concepto de Modelo (MVC). Desde el Modelo se trae y manipula la información de la BD.
- Layouts and Rendering: Encargado de la renderización de vistas o cualquier estructura gráfica.
- ActionView: Ayuda a agilizar la inserción de componentes gráficos mediante Helpers.
- ActionController: Se utiliza cuando se realizan las peticiones al servidor desde el cliente.
- RailsRouting: Facilita el manejo de las rutas.
- ActionMailer: Usado para enviar e-mails.
- The Assets Pipeline: Manejo de assets de la aplicación.
- Internationalization i18n: Provee un sistema para abordar el tema de la internacionalización y el uso de varios idiomas.
- Rails + JS: Ayuda a que sea posible usar código JS en un proyecto de RoR.

[![img](https://www.google.com/s2/favicons?domain=https://atom.io//favicon.ico)Atom](https://atom.io/)

[![img](https://www.google.com/s2/favicons?domain=https://www.ruby-lang.org/en/downloads//favicon.ico)Download Ruby](https://www.ruby-lang.org/en/downloads/)

[![img](https://www.google.com/s2/favicons?domain=https://guides.rubyonrails.org/images/favicon.ico)Ruby on Rails Guides](https://guides.rubyonrails.org/)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)Arquitecturas de Software en Android: MVC, MVP y MVVM](https://platzi.com/blog/arquitecturas-de-software-en-android-mvc-mvp-y-mvvm/)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Manifiesto por el Desarrollo Ágil de Software](https://agilemanifesto.org/iso/es/manifesto.html)

## Instalación de Ruby, RoR en Linux

### Instalación Entorno de Desarrollo

#### Objetivo

Instalar el entorno de desarrollo necesario para desplegar aplicaciones con Ruby on Rails incluyendo librerías, consola de comandos, base de datos y editor de texto en ambientes de escritorio compatibles con linux, usando **Ubuntu 20.04 LTS**.

#### Recomendaciones

No copies y pegues los comandos de la guía dado que en algunas ocasiones podrían tener caracteres especiales de formato que podrían dañar la ejecución de los mismos, por otro lado, la instalación se realiza sobre un sistema operativo recién instalado, si tienes una instalación previa puedes encontrar que algunas respuestas de tus comandos sean diferentes.

#### Convenciones

**$** *(consola de comandos) debes traspasar el comando sin el símbolo $*

**#=>** *(salida de comando)*

**[]** *(Opcional)*

***`<consola cursiva>`\*** *(texto que hay que añadir o quitar acorde a la explicación del paso)*

NOTA IMPORTANTE

No debes usar el usuario **superusuario** (root) a menos que de forma explícita se especifique que este debe ser usado a través del comando “sudo”, de otra forma no debes usarlo así como tampoco sus variaciones como “$ sudo su”

El programa “sudo” pronunciado “SUDU” /suːduː/ es normalmente interpretado como “superuser do…” y permite al comando inmediatamente siguiente, correr con los permisos de otro usuario que tiene condiciones especiales de seguridad o permisos, comúnmente **sudo** se usa para invocar los permisos del superusuario del sistema operativo, sin embargo, en las últimas versiones de sistemas compatibles con Linux no sólo puede ejecutar como superusuario sino también es posible usar otros tipos especiales de usuario. Sin embargo, nosotros usaremos “sudo” para usar los permisos de superusuario.

### Adecuación del sistema operativo

#### Instalación de librerías base

Usando sudo y y el comando apt-get (como gestor de paquetes) vas a actualizar los enlaces a los repositorios de las librerías que se pueden instalar en el sistema operativo así como sus dependencias.

```sh
$ sudo apt-get update
```

Deberás obtener una respuesta de varias líneas al comando con la actualización de la lista de paquetes y una línea final mencionado que la lectura de las listas de paquetes ha sido finalizada similar a lo siguiente:

```sh
Reading package lists... Done
```

Ahora vamos a instalar las librerias necesarias usando **sudo** y el comando **apt install**

```sh
$ sudo apt install build-essential curl wget openssl libssl-dev libreadline-dev dirmngr zlib1g-dev libmagickwand-dev imagemagick-6.q16 libffi-dev libpq-dev cmake libwebp-dev
```

Para autorizar la instalación deberás aceptar la continuación del proceso digitando la letra **Y** y dando **enter**

```sh
Do you want to continue? [Y/n]
```

Las librerías instaladas cumplirán con los siguientes propósitos:

- **build-essentials**: contiene herramientas para compilar y construir software desde sus fuentes usando generalmente lenguaje C y sus derivados
- **curl**: es una herramienta para transferir información de un servidor a otro usando diversos tipos de protocolos, entre esos: HTTP, FTP, IMAP entre otros
- **wget**: es una herramienta para recibir contenido desde la web usando los protocolos más comunes como HTTP y FTP
- **openssl**: es un robusto conjunto de librerías que te permitirán manipular y leer contenido de los protocolos TLS y SSL, que son usados para garantizar la seguridad y encriptación de las comunicaciones entre servidores y clientes.
- **libssl-dev**: es una librería que forma parte de OpenSSL usada para lidiar con procesos de encriptación, este paquete, contiene librerías de desarrollo, cabeceras de compilación entre otro tipos de archivos.
- **libreadline-dev**: aporta la librerías de desarrollo para el paquete readline que ayuda a la consistencia de interfaces de usuario que están asociadas a líneas interactivas de comandos.
- **dirmngr**: es un pequeño servidor para gestionar y descargar certificados de tipo X.509 y la lista de revocación de los mismos.
- **zlib1g-dev**: aporta librerías de desarrollo para soportar mecanismos de compresión y descompresión compatibles con GZIP y PKZIP, tecnologías de compresión comunes en paquetes de herramientas para nuestro entorno.
- **imagemagick-6.q16**: es un programa que permite editar, crear y componer mapas de bits en imágenes.
- **libmagickwand-dev**: es un conjunto de librerías de desarrollo para compilar librerías necesarias para el uso de MagickWand, este último es la tecnología predilecta como interface del lenguaje de programación C a ImageMagick.
- **libffi-dev**: provee librerías de desarrollo para habilitar mecanismos de alto nivel para el uso de *calling conventions* sobre ciertos compiladores, de esta forma esta librería le permite al desarrollador invocar cualquier función específica por medio de una llamada en tiempo de ejecución.
- **libpq-dev**: esta librería contiene paquetes de desarrollo para habilitar el uso de conjuntos de binarios y cabeceras requeridas para construir componentes externos para PostgreSQL
- **cmake**: es usado para controlar el proceso de compilación de software usando un método independiente del compilador.
- **libwebp-dev**: habilita un conjunto de librerías de desarrollo para el manejo de formatos de imágenes de nueva generación compatibles con WebP

### Instalación de GIT

A continuación instalaremos nuestro sistema para controlar versiones de nuestro código, para este caso usaremos GIT, y procederemos a su instalación usando el siguiente comando

```sh
$ sudo apt install git
```

Una vez instalado GIT configuraremos de forma global algunas variables

```sh
$ git config --global user.name "Johan Tique"

$ git config --global user.email johan@platzi.com

$ git config --global color.ui true
```

**Solo si** tu conexión a internet está habilitada a través de un proxy (muy común en las instituciones públicas) debemos configurarlo de la siguiente manera

```sh
$ git config --global http.proxy http://proxy.alu.uma.es:3128

$ git config --global https.proxy https://proxy.alu.uma.es:3128
```

### Rockeando con tu consola de comandos

Por defecto nuestro sistema operativo está usando la consola BASH, que es genial, sin embargo, vamos usar un entorno aún más productivo integrando una nueva consola y un framework para configurar la misma.

La nueva consola que usaremos será ZSH, y el framework de configuración será oh-my-zsh (pronunciado <o mai si shel> [əʊ maɪ siː ʃɛl]); primero instalaremos la consola de la siguiente forma:

```sh
$ sudo aptitude install zsh
```

Luego instalaremos el framework oh-my-zsh con la ejecución comando de abajo, durante el proceso de instalación te va a preguntar si deseas cambiar tu consola por defecto a ZSH, a lo que debes responder afirmativamente

```sh
$ sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

A continuación te mostraré una respuesta completa de este comando (debes ver esto después de la ejecución del comando incluyendo la pregunta de cambio de consola)

```sh
Time to change your default shell to zsh:

**Do you want to change your default shell to zsh? [Y/n]** y

Changing the shell...

Password:

Shell successfully changed to '/usr/bin/zsh'.

__ __

____ / /_ ____ ___ __ __ ____ _____/ /_

/ __ \/ __ \ / __ `__ \/ / / / /_ / / ___/ __ \

/ /_/ / / / / / / / / / / /_/ / / /_(__ ) / / /

\____/_/ /_/ /_/ /_/ /_/\__, / /___/____/_/ /_/

/____/ ....is now installed!

  
Please look over the ~/.zshrc file to select plugins, themes, and options.

p.s. Follow us on https://twitter.com/ohmyzsh
  
p.p.s. Get stickers, shirts, and coffee mugs at https://shop.planetargon.com/collections/oh-my-zsh
```

Al final de tu proceso deberás ver tu consola con un nuevo estilo como el siguiente:

```sh
➜ ~
```

Para corroborar que el proceso se finalizó con normalidad deberás cerrar tu sesión de usuario y volver a ingresar, y abrir tu consola de comandos, esta deberá verse de la siguiente forma:

![unnamed.png](https://static.platzi.com/media/user_upload/unnamed-518d08d4-3b94-4ace-be89-b84dcb160962.jpg)

Después de haber instalado el framework y la consola con éxito, vamos a fortalecer nuestro framework oh-my-zsh vinculando algunos plugins, el primero de ellos lo debemos descargar, se trata del plugin zsh-syntax-highlighting.

Para descargarlo e instalarlo debemos ir al directorio de plugins de oh-my-zsh y clonar el repositorio del plugin

```sh
➜ ~ cd ~/.oh-my-zsh/custom/plugins

➜ ~ git clone git://github.com/zsh-users/zsh-syntax-highlighting.git
```

Para proceder con la habilitación de los plugins incluyendo el anterior, vamos a acceder al archivo `.zshrc` (archivo de run commands de zsh) y desde allí buscar la línea donde a través de un arreglo será pasados todos los plugins que queremos que sean habilitados, normalmente es la línea 71 y comienza con el texto `plugins=(git)`

```sh
➜ ~ cd
➜ ~ gedit ~/.zshrc
```

Dentro de este archivo, reemplazamos la línea anteriormente mencionada por lo siguiente:

```sh
plugins=(git bundler colorize brew zeus gem rails ruby npm node history-substring-search zsh-syntax-highlighting)
```

Finalmente, en el mismo archivo pero al final de este, vamos a añadir **una** de estas dos opciones:

Si usas lenguaje inglés y tu teclado está en inglés añadirás lo siguiente:

```sh
# Setting up a clean UTF-8 environment

  
export LANGUAGE=en_US.UTF-8

  
export LC_ALL=en_US.UTF-8
```

Si usas lenguaje español y tu teclado está en español añadirás lo siguiente:

```sh
# Setting up a clean UTF-8 environment

  
export LANGUAGE=es_CO.UTF-8

  
export LC_ALL=es_CO.UTF-8
```

Esta opción nos permitirá tener claridad sobre la codificación de caracteres usada en nuestro entorno. Para guardar toda la configuración generada debes usar CTRL + S si estás usando gedit como editor de texto.

Para actualizar las configuraciones generadas en tu archivo `.zshrc` tendrás dos opciones o cierras y vuelves a abrir tu consola de comandos, o en la raíz de tu directorio de usuario deberás ejecutar el siguiente comando:

```sh
➜ ~ source ~/.zshrc
```

Hasta aquí no deberías tener ningún inconveniente, sin embargo, sólo si llegas a obtener algún error similar a `compdef: unknown command or service: rails` deberás hacer lo siguiente

```sh
➜ ~ chmod -R go-w ~/.oh-my-zsh

➜ ~ rm -f ~/.zcompdump*; compinit
```

### Instalando RBENV

Imagina que estás trabajando en varios proyectos con Ruby y Ruby on Rails, y que todos tengan diferentes versiones… **¿cómo haces para garantizar que todas librerias, versiones y paquetes sean consistentes en cada uno de los proyectos?**

Bien, para resolver esto debemos usar gestores de versiones, en este caso usaremos RBENV, no debes confundirte con controladores de versiones (como GIT), son dos tecnologías con propósitos distintos, pero que se complementan.

Para instalar RBENV debemos ejecutar el siguiente comando

```sh
➜ ~ curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-installer | bash
```

Al final de la ejecución del comando deberás ver un texto similar a este, mencionando que a pesar de haber instalado RBENV este no se encuentra en la variable de entorno PATH

```sh
Running doctor script to verify installation...

Checking for `rbenv' in PATH: not found

You seem to have rbenv installed in `/home/platzi/.rbenv/bin', but that

directory is not present in PATH. Please add it to PATH by configuring

your `~/.bashrc', `~/.zshrc', or `~/.config/fish/config.fish'.
```

Para añadir a la variable de entorno PATH los binarios de RBENV y a su vez ejecutarlo cada vez que exista una nueva sesión, debemos agregar dos líneas al archivo .zshrc que habíamos visto antes, pero esta vez solo usaremos la consola para hacerlo, de la siguiente manera:

```sh
➜ ~ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.zshrc

➜ ~ echo 'eval "$(rbenv init -)"' >> .zshrc

➜ ~ source ~/.zshrc
```

Una vez instalado RBENV vamos a usarlo para instalar la versión 2.7.1 ejecutando lo siguiente (la instalación suele tardar unos minutos, así que asegurate de tener una buena velocidad y estabilidad de internet)

```sh
➜ ~ rbenv install 2.7.1
```

RBENV está en la capacidad de gestionar un sin número de versiones de ruby así como sus espacios de librerías de forma independiente, así que vamos a establecer la versión 2.7.1 como la versión por defecto y global, usando el siguiente comando.

```sh
➜ ~ rbenv global 2.7.1
```

Para poder mantener los espacios de librerías de forma consistente entre versiones y manifiestos usaremos BUNDLER, la cual es una librería que nos permitirá gestionar a su vez un conjunto de librerías de ruby agrupadas por entornos de trabajo y garantizando la consistencia de versiones y dependencias entre las mismas.

En Ruby, las librerías son llamadas gemas, y bundler usa un archivo manifiesto llamado Gemfile para listar todas las gemas que un proyecto tendrá, bundler puede usar varias fuentes para encontrar de forma pública estas librerías en la nube, sin embargo, nosotros vamos a usar dos fuentes rubygems y github.

Para instalar Bundler (que también es una gema) usaremos el comando `gem` que es proveído en este caso por RBENV. También habilitaremos el protocolo seguro de HTTP para que cuando se tome github como fuente este sea establecido por defecto.

```sh
➜ ~ gem install bundler

➜ ~ bundle config github.https true
```

Rails necesita de un motor de javascript para abordar ciertas funcionalidades de compilación, transformación y en sus últimas versiones, funcionalidades de integración con webpack, es por esta razón que vamos a usar **nodejs** como nuestro motor, sin embargo, NodeJS podría tener la misma situación de conflicto de versiones entre ambientes, por lo que que usaremos también un gestion de versiones de NodeJS para lidiar con estos escenarios.

**NOTA** si ya tienes instalado NodeJS u otro motor de javascript compatible con el interprete de javascript V8 o similar, no deberás hacer este paso, aunque es ampliamente recomendable usar un gestor de versiones, si lo quieres llegar a usar debes desinstalar el intérprete de javascript que tengas instalado para evitar conflictos futuros.

Para gestionar la versiones de NodeJS usaremos NODENV ejecutando la siguiente secuencia de comandos, que instalará el NODENV, añadirá sus binarios a la variable de entorno PATH, e instalará la versión 12.17.0 de NodeJS.

```sh
➜ ~ curl -fsSL https://raw.githubusercontent.com/nodenv/nodenv-installer/master/bin/nodenv-installer | bash

➜ ~ echo 'export PATH="$HOME/.nodenv/bin:$PATH"' >> ~/.zshrc

➜ ~ echo 'eval "$(nodenv init -)"' >> ~/.zshrc

➜ ~ source ~/.zshrc

➜ ~ nodenv install 12.17.0

➜ ~ nodenv global 12.17.0
```

### Instalando el ambiente de desarrollo con Rails y PostgreSQL

Habiendo instalado RBENV y BUNDLER ya podemos empezar a instalar todas las gemas, herramientas y servidores necesarios para poder seguir adelante con nuestros primeros pasos con Ruby on Rails

Vamos a instalar Ruby on Rails usando el siguiente comando (si, Rails es también una gema…)

```sh
➜ ~ gem install rails
```

Rails instalará con varias gemas como dependencias para su ejecución, así que obtendremos una instalación con una larga lista de librerías, al final podrás corroborar la versión de Rails usando el comando de abajo, que en nuestro caso será la versión `6.0.3.1`

```sh
➜ ~ rails -v

#=> Rails 6.0.3.1
```

Las versiones de Rails superiores a la 6, requieren usar YARN como gestor de paquetes de javascript y habilitar el enlace de Webpack usando la tecnología Webpacker, para esto, vamos a usar la versión oficial de los repositorios de YARN a través de los siguientes comandos

```sh
➜ ~ sudo curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

➜ ~ sudo sh -c "echo 'deb https://dl.yarnpkg.com/debian/ stable main' >> /etc/apt/sources.list"

➜ ~ sudo apt update
```

Para instalar YARN vamos a usar el comando de abajo, y usaremos nuestra versión de NodeJS instalada en el paso anterior usando la opción `--no-install-recommends`

```sh
➜ ~ sudo apt --no-install-recommends install yarn
```

Finalmente, vamos a instalar la base de datos de postgreSQL y configurar un usuario para poder gestionarla, lo primero que debes hacer es configurar las listas de los repositorios

```sh
➜ ~ wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

➜ ~ echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list

➜ ~ sudo apt update
```

Una vez actualizado la lista de paquetes de postgreSQL usaremos el siguiente comando para instalarlo

```sh
➜ ~ sudo apt -y install postgresql-12 postgresql-client-12
```

Una vez instalado postgreSQL, entraremos a la sesión del mismo y crearemos desde allí un nuevo usuario con capacidad de crear bases de datos (de usuario **platzi**, rol **platzi** y contraseña **platzi**), y que usaremos para nuestras futuras aplicaciones, para hacerlo debemos seguir el siguiente conjunto de instrucciones (**No** tener en cuenta el prefijo `postgres@ubuntu:~$` para su ejecución):

```sh
➜ ~ sudo -i -u postgres

postgres@ubuntu:~$ createuser --pwprompt --interactive platzi
  

#=> Enter password for new role: ****** (platzi)

#=> Enter it again: ******

#=> Shall the new role be a superuser? (y/n) n

#=> Shall the new role be allowed to create databases? y

#=> Shall the new role be allowed to create more new roles? (y/n) n
```

Para salir de la sesión de postgreSQl debemos usar el comando `exit`

```sh
postgres@ubuntu:~$ exit
```

### Instalación del editor de texto

Vamos a instalar el editor de texto **Atom**, primero añadiremos sus listas de paquetes oficiales y luego procederemos a su instalación

```sh
➜ ~ wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -

➜ ~ sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'

➜ ~ sudo apt-get update

➜ ~ sudo apt-get install atom
```

Una vez instalado nuestro editor de texto, iremos a la raíz de nuestra consola de comandos, y crearemos los directorios `coding/platzi` con el comando mkdir (make directory), la opción -p nos permite crear directorios anidados de forma concatenada si estos no existen previamente.

```sh
➜ ~ mkdir -p coding/platzi
```

Accederemos dentro de este directorio, para posteriormente invocar al editor a través del siguiente comando

```sh
➜ ~ coding/platzi

➜ ~ atom .
```

Al ejecutar el comando deberás ver la interfaz gráfica del editor de la siguiente manera

![atom.png](https://static.platzi.com/media/user_upload/atom-71e98994-a7e1-4557-b6b0-e5fd7d1ec355.jpg)

Finalmente vamos a instalar y habilitar algunos paquetes que complementarán tus procesos de desarrollo.

**Paquete autosave**

Este paquete debemos habilitarlo, para hacerlo debemos sobre el Atom usar la combinación de teclas **CTRL + ,** clickear la opción **Packages**, para luego buscar el paquete autosave, seleccionarlo y finalmente habilitarlo.

![rubyyy.png](https://static.platzi.com/media/user_upload/rubyyy-bc00ebe9-c787-4747-84fe-205ffd76f1e4.jpg)

**Instalación de paquetes**

En la misma pantalla de de configuración, ahora debes clickear **install** y allí procederemos a buscar e instalar cada uno de los siguientes paquetes:

atom-beautify, file-icons, git-blame, ident-guide-improved, language-haml, minimap, pigments, sass, save-workspace, selection-highlight, todo-show.

![atom-beautify, file-icons, git-blame, ident-guide-.png](https://static.platzi.com/media/user_upload/atom-beautify%2C%20file-icons%2C%20git-blame%2C%20ident-guide--bd19378e-48c3-4ee7-b8ac-7c0993abc75b.jpg)

### 

### BONUS atajos de teclado y comandos

Para la consola de comando podrás usar los siguientes comandos y atajos, los cuales iremos abordando en el transcurso del curso

```sh
# gestión de archivos y carpetas

$ cd *<nombre_de_carpeta>* # ingresar a un directorio

$ ls # ver el contenido del directorio

$ cp *<nombre_archivo>* *<nombre_archivo_2>* # copiar un archivo especificando sus nombres

# GIT

$ ga . # atajo de `git add .`

$ gc -m ‘’ # atajo de `git commit -m ‘’`

$ gco # atajo de `git checkout`

$ gp # atajo de `git push`

# Rails

$ rs # atajo de `rails server`

$ rc # atajo de `rails console`

$ bi # atajo de bundle install
```

Dentro del editor de texto Atom puedes encontrar útil estos comandos

```sh
CTRL + P # Buscar archivos y carpetas dentro del proyecto

CTRL + SHIFT + P # Buscar comando derivado de los paquetes instalados

CTRL + K + B # collapsar o expandir el arbol de directorio

CTRL + D # ejecutado varias veces, seleccionará las coincidencias del término enfocado

CTRL + SHIFT + D # duplicar línea

CTRL + <flechas arriba/abajo> # mover la línea hacia abajo o arriba

ALT + \ # enfocar y navegar el árbol de directorios

SHIFT + A # enfocado el árbol de directorios, creará una carpeta

A # enfocado el árbol de directorios, creará un archivo

D # enfocado el árbol de directorios, duplicará un archivo

F2 # enfocado el árbol de directorios, renombrará un archivo

CTRL + F # buscar un término dentro del archivo abierto

CTRL + SHIFT + F # buscar un término en todo el proyecto
```

nstalación de RBENV

```sh
curl -fsSL https://github.com/rbenv/rbenv-installer/raw/HEAD/bin/rbenv-installer | bash
```

## Instalación de Ruby, RoR en Mac y Windows

### Objetivo

Instalar el entorno de desarrollo necesario para desplegar aplicaciones con Ruby on Rails incluyendo gestores de versiones, consola de comandos y base de datos en computadores mac usando macOS 10.15 Catalina y Windows 10.

### Recomendaciones

No copies y pegues los comandos de la guía dado que en algunas ocasiones podrían tener caracteres especiales de formato que podrían dañar la ejecución de los mismos, por otro lado, la instalación se realiza sobre un sistema operativo recién instalado, si tienes una instalación previa puedes encontrar que algunas respuestas de tus comandos sean diferentes.

### Convenciones

**~ %** *(consola de comandos) (debes traspasar el comando sin el símbolo “~ %”)*

**#=>** *(salida de comando)*

**[]** *(Opcional)*

***``\****(texto que hay que añadir o quitar acorde a la explicación del paso)*

### Instalación de Ruby, RoR en Mac

#### Introduccion

Al igual que en la instalación del entorno para Linux, vamos a usar el framework de configuración oh-my-zsh aprovechando además que en Catalina, nuestra shell por defecto es zsh. Para hacerlo debes en la terminal de comandos colocar la siguiente instrucción, que encontrarás en la página oficial de github de oh-my-zsh https://github.com/ohmyzsh/ohmyzsh#via-curl

```sh
~ % sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

A continuación te mostraré una respuesta completa de este comando (debes ver esto después de la ejecución del comando incluyendo la pregunta de cambio de consola)

```sh
__ __

____ / /_ ____ ___ __ __ ____ _____/ /_

/ __ \/ __ \ / __ `__ \/ / / / /_ / / ___/ __ \

/ /_/ / / / / / / / / / / /_/ / / /_(__ ) / / /

\____/_/ /_/ /_/ /_/ /_/\__, / /___/____/_/ /_/

/____/ ....is now installed!


Please look over the ~/.bashrc file to select plugins, themes, and options.

p.s. Follow us on https://twitter.com/ohmyzsh

p.p.s. Get stickers, shirts, and coffee mugs at https://shop.planetargon.com/collections/oh-my-zsh
```

Al final de tu proceso deberás ver tu consola con un nuevo estilo como el siguiente:

```sh
➜ ~
```

Después de instalar oh-my-zsh debemos instalar un gestor de paquetes, en nuestro caso usaremos Homebrew (ampliamente usado en el mundo de desarrollo de software), puedes seguir las indicaciones de la instalación desde la siguiente pagina https://brew.sh/ y digitar la siguiente instruccion:

```sh
➜ ~ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

Una vez instalado homebrew usamos rbenv como gestor de versiones de Ruby, de igual manera que lo hicimos para la instalación del ambiente de Linux (puedes ir la sección Instalando RBENV en la lectura **Instalación Entorno de Desarrollo** para que obtengas más información).

```sh
➜ ~ brew install rbenv ruby-build
```

Luego debemos añadir al archivo `.bashrc` una configuración de inicialización del RBENV

```sh
➜ ~ echo 'if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi' >> ~/.bashrc

➜ ~ source ~/.bashrc
```

Una vez instalado RBENV vamos a proceder a instalar la versión de ruby 2.7.1 y a establecerla por defecto

```sh
➜ ~ rbenv install 2.7.1
➜ ~ rbenv global 2.7.1
```

Ahora, vamos a configurar GIT usando nuestro nombre y cuenta de correo habitual

```sh
$ git config --global user.name "Johan Tique"
$ git config --global user.email johan@platzi.com
$ git config --global color.ui true
```

Para poder mantener los espacios de librerías de forma consistente entre versiones y manifiestos usaremos BUNDLER, la cual es una librería que nos permitirá gestionar a su vez un conjunto de librerías de ruby agrupadas por entornos de trabajo y garantizando la consistencia de versiones y dependencias entre las mismas.

En Ruby, las librerías son llamadas gemas, y bundler usa un archivo manifiesto llamado Gemfile para listar todas las gemas que un proyecto tendrá, bundler puede usar varias fuentes para encontrar de forma pública estas librerías en la nube, sin embargo, nosotros vamos a usar dos fuentes rubygems y github.

Para instalar Bundler (que también es una gema) usaremos el comando `gem` que es proveído en este caso por RBENV. También habilitaremos el protocolo seguro de HTTP para que cuando se tome github como fuente este sea establecido por defecto.

```sh
$ gem install bundler
$ bundle config github.https true
```

Rails necesita de un motor de javascript para abordar ciertas funcionalidades de compilación, transformación y en sus últimas versiones, funcionalidades de integración con webpack, es por esta razón que vamos a usar **nodejs** como nuestro motor, sin embargo, NodeJS podría tener la misma situación de conflicto de versiones entre ambientes, por lo que que usaremos también un gestion de versiones de NodeJS para lidiar con estos escenarios.

**NOTA** si ya tienes instalado NodeJS u otro motor de javascript compatible con el intérprete de javascript V8 o similar, no deberás hacer este paso, aunque es ampliamente recomendable usar un gestor de versiones, si lo quieres llegar a usar debes desinstalar el intérprete de javascript que tengas instalado para evitar conflictos futuros.

Para gestionar las versiones de NodeJS usaremos NODENV ejecutando la siguiente secuencia de comandos, que instalará el NODENV, añadirá sus binarios a la variable de entorno PATH, e instalará la versión 12.17.0 de NodeJS.

```sh
$ brew install nodenv
$ echo 'eval "$(nodenv init -)"' >> ~/.bashrc
$ source ~/.bashrc
$ nodenv install 12.17.0
$ nodenv global 12.17.0
```

### Instalando el ambiente de desarrollo con Rails y PostgreSQL

Habiendo instalado RBENV y BUNDLER ya podemos empezar a instalar todas las gemas, herramientas y servidores necesarios para poder seguir adelante con nuestros primeros pasos con Ruby on Rails

Vamos a instalar Ruby on Rails usando el siguiente comando (si, Rails es también una gema)

```sh
$ gem install rails
```

Rails instalará con varias gemas como dependencias para su ejecución, así que obtendremos una instalación con una larga lista de librerías, al final podrás corroborar la versión de Rails usando el comando de abajo, que en nuestro caso será la versión `6.0.3.2`

```sh
$ rails -v
#=> Rails 6.0.3.2
```

Las versiones de Rails superiores a la 6, requieren usar YARN como gestor de paquetes de javascript y habilitar el enlace de Webpack usando la tecnología Webpacker, para esto, vamos a usar nuevamente Homebrew

```sh
$ brew install yarn
```

Finalmente, vamos a instalar la base de datos de postgreSQL y configurar un usuario para poder gestionarla

```sh
$ brew install postgresql
```

Una vez instalado iniciaremos el servicio de la base de datos

```sh
$ brew services start postgresql
```

Una vez inicializado postgreSQL, entraremos a la sesión del mismo y crearemos desde allí un nuevo usuario con capacidad de crear bases de datos (de usuario **platzi**, rol **platzi** y contraseña **platzi**), y que usaremos para nuestras futuras aplicaciones, para hacerlo debemos seguir el siguiente conjunto de instrucciones (**No** tener en cuenta el prefijo `postgres=#` para su ejecución):

```sh
$ psql postgres
postgres=# CREATE ROLE platzi WITH LOGIN PASSWORD 'platzi';
postgres=# ALTER ROLE platzi CREATEDB;
```

Para salir de la sesión de postgreSQl debemos usar el comando `\q`

## Instalación de Ruby, RoR en Windows 10

La instalación del entorno de desarrollo en Windows la haremos usando un subsistema de windows para Linux, de esta forma podrás usar una consola tipo Linux que te facilitara la ejecución de instrucciones y hará que tu sistema operativo sea más compatible con la documentación que encuentres en este curso y en la internet.

Como te he mencionado arriba, Windows 10 te permite correr una virtualización nativa de Linux , y usaremos este entorno para instalar todas nuestras herramientas, para preparar nuestro sistema operativo debemos abrir un PowerShell y ejecutalo como administrador como es referenciado en la imagen de abajo.

Una vez allí, ejecuta las siguientes tres instrucciones

```sh
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart

dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
```

Seguido de esto, vamos a abrir el Microsoft Store, buscar el paquete de Ubuntu 20.04 LTS e instalarlo

![img](https://lh3.googleusercontent.com/8mWDbSkNwJSZfAqRLxeU7Q-r9510AjYd-T_0RPQTaGvDyydDWXWFp1yZuHoag0dNYzJnrj-_WNmpODUKhhZocm97BPWxaFIOOU85P7Qx-vEHtbWwnO517wWrbAfOy_-TcaDyXDTJ)

Una vez instalado, debemos buscar y ejecutar el paquete Ubuntu 20.04 desde el buscador de Windows, al inicio este nos solicitara que creemos un usuario y una nueva contraseña, para nuestro caso será **platzi**

![img](https://lh5.googleusercontent.com/RYHtBC-jORbM8Pbn3j8C7okYDljBDxILoEY2wLXc84GzsZ9btjihO8ngL-Xui5hwwjP-cO1feDOi7dz1Me1vYLI-kKwWS_4cthfFqECJ-c6DdcWvs1LKCq1f-O5N0yp-Frj2_jOm)

### NOTA IMPORTANTE

No debes usar el usuario **superusuario** (root) a menos que de forma explícita se especifique que este debe ser usado a través del comando “sudo”, de otra forma no debes usarlo así como tampoco sus variaciones como “$ sudo su”

El programa “sudo” pronunciado “SUDU” /suːduː/ es normalmente interpretado como “superuser do…” y permite al comando inmediatamente siguiente, correr con los permisos de otro usuario que tiene condiciones especiales de seguridad o permisos, comúnmente **sudo** se usa para invocar los permisos del superusuario del sistema operativo, sin embargo, en las últimas versiones de sistemas compatibles con Linux no sólo puede ejecutar como superusuario sino también es posible usar otros tipos especiales de usuario. Sin embargo, nosotros usaremos “sudo” para usar los permisos de superusuario.

### Adecuación del sistema operativo

#### Instalación de librerías base

Usando sudo y y el comando apt-get (como gestor de paquetes) vas a actualizar los enlaces a los repositorios de las librerías que se pueden instalar en el sistema operativo así como sus dependencias.

```sh
$ sudo apt-get update
```

Deberás obtener una respuesta de varias líneas al comando con la actualización de la lista de paquetes y una línea final mencionado que la lectura de las listas de paquetes ha sido finalizada similar a lo siguiente:

```sh
Reading package lists... Done
```

Ahora vamos a instalar las librerias necesarias usando **sudo** y el comando **apt install**

```sh
$ sudo apt install build-essential curl wget openssl libssl-dev libreadline-dev dirmngr zlib1g-dev libmagickwand-dev imagemagick-6.q16 libffi-dev libpq-dev cmake libwebp-dev
```

Para autorizar la instalación deberás aceptar la continuación del proceso digitando la letra **Y** y dando **enter**

```sh
Do you want to continue? [Y/n]
```

Las librerías instaladas cumplirán con los siguientes propósitos:

- **build-essentials:** contiene herramientas para compilar y construir software desde sus fuentes usando generalmente lenguaje C y sus derivados
- **curl:** es una herramienta para transferir información de un servidor a otro usando diversos tipos de protocolos, entre esos: HTTP, FTP, IMAP entre otros
- **wget:** es una herramienta para recibir contenido desde la web usando los protocolos más comunes como HTTP y FTP
- **openssl:** es un robusto conjunto de librerías que te permitirán manipular y leer contenido de los protocolos TLS y SSL, que son usados para garantizar la seguridad y encriptación de las comunicaciones entre servidores y clientes.
- **libssl-dev:** es una librería que forma parte de OpenSSL usada para lidiar con procesos de encriptación, este paquete, contiene librerías de desarrollo, cabeceras de compilación entre otro tipos de archivos.
- **libreadline-dev:** aporta la librerías de desarrollo para el paquete readline que ayuda a la consistencia de interfaces de usuario que están asociadas a líneas interactivas de comandos.
- **dirmngr:** es un pequeño servidor para gestionar y descargar certificados de tipo X.509 y la lista de revocación de los mismos.
- **zlib1g-dev:** aporta librerías de desarrollo para soportar mecanismos de compresión y descompresión compatibles con GZIP y PKZIP, tecnologías de compresión comunes en paquetes de herramientas para nuestro entorno.
- **imagemagick-6.q16:** es un programa que permite editar, crear y componer mapas de bits en imágenes.
- **libmagickwand-dev:** es un conjunto de librerías de desarrollo para compilar librerías necesarias para el uso de MagickWand, este último es la tecnología predilecta como interface del lenguaje de programación C a ImageMagick.
- **libffi-dev:** provee librerías de desarrollo para habilitar mecanismos de alto nivel para el uso de *calling conventions* sobre ciertos compiladores, de esta forma esta librería le permite al desarrollador invocar cualquier función específica por medio de una llamada en tiempo de ejecución.
- **libpq-dev:** esta librería contiene paquetes de desarrollo para habilitar el uso de conjuntos de binarios y cabeceras requeridas para construir componentes externos para PostgreSQL
- **cmake:** es usado para controlar el proceso de compilación de software usando un método independiente del compilador.
- **libwebp-dev:** habilita un conjunto de librerías de desarrollo para el manejo de formatos de imágenes de nueva generación compatibles con WebP

### Instalación de GIT

A continuación instalaremos nuestro sistema para controlar versiones de nuestro código, para este caso usaremos GIT, y procederemos a su instalación usando el siguiente comando

```sh
$ sudo apt install git
```

Una vez instalado GIT configuraremos de forma global algunas variables

```sh
$ git config --global user.name "Johan Tique"
$ git config --global user.email johan@platzi.com
$ git config --global color.ui true
```

**Solo si** tu conexión a internet está habilitada a través de un proxy (muy común en las instituciones públicas) debemos configurarlo de la siguiente manera

```sh
$ git config --global http.proxy http://proxy.alu.uma.es:3128
$ git config --global https.proxy https://proxy.alu.uma.es:3128
```

### Instalando RBENV

Imagina que estás trabajando en varios proyectos con Ruby y Ruby on Rails, y que todos tengan diferentes versiones… **¿cómo haces para garantizar que todas librerias, versiones y paquetes sean consistentes en cada uno de los proyectos?**

Bien, para resolver esto debemos usar gestores de versiones, en este caso usaremos RBENV, no debes confundirte con controladores de versiones (como GIT), son dos tecnologías con propósitos distintos, pero que se complementan.

Para instalar RBENV debemos ejecutar el siguiente comando

```sh
$ curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-installer | bash
```

Al final de la ejecución del comando deberás ver un texto similar a este, mencionando que a pesar de haber instalado RBENV este no se encuentra en la variable de entorno PATH

```sh
Running doctor script to verify installation...
Checking for `rbenv' in PATH: not found
You seem to have rbenv installed in `/home/platzi/.rbenv/bin', but that
directory is not present in PATH. Please add it to PATH by configuring

your `~/.bashrc', `~/.bashrc', or `~/.config/fish/config.fish'.
```

Para añadir a la variable de entorno PATH los binarios de RBENV y a su vez ejecutarlo cada vez que exista una nueva sesión, debemos agregar dos líneas al archivo .bashrc que habíamos visto antes, pero esta vez solo usaremos la consola para hacerlo, de la siguiente manera:

```sh
$ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
$ echo 'eval "$(rbenv init -)"' >> .bashrc
$ source ~/.bashrc
```

Una vez instalado RBENV vamos a usarlo para instalar la versión 2.7.1 ejecutando lo siguiente (la instalación suele tardar unos minutos, así que asegurate de tener una buena velocidad y estabilidad de internet)

```sh
$ rbenv install 2.7.1
```

RBENV está en la capacidad de gestionar un sin número de versiones de ruby así como sus espacios de librerías de forma independiente, así que vamos a establecer la versión 2.7.1 como la versión por defecto y global, usando el siguiente comando.

```sh
$ rbenv global 2.7.1
```

Para poder mantener los espacios de librerías de forma consistente entre versiones y manifiestos usaremos BUNDLER, la cual es una librería que nos permitirá gestionar a su vez un conjunto de librerías de ruby agrupadas por entornos de trabajo y garantizando la consistencia de versiones y dependencias entre las mismas.

En Ruby, las librerías son llamadas gemas, y bundler usa un archivo manifiesto llamado Gemfile para listar todas las gemas que un proyecto tendrá, bundler puede usar varias fuentes para encontrar de forma pública estas librerías en la nube, sin embargo, nosotros vamos a usar dos fuentes rubygems y github.

Para instalar Bundler (que también es una gema) usaremos el comando `gem` que es proveído en este caso por RBENV. También habilitaremos el protocolo seguro de HTTP para que cuando se tome github como fuente este sea establecido por defecto.

```sh
$ gem install bundler
$ bundle config github.https true
```

Rails necesita de un motor de javascript para abordar ciertas funcionalidades de compilación, transformación y en sus últimas versiones, funcionalidades de integración con webpack, es por esta razón que vamos a usar **nodejs** como nuestro motor, sin embargo, NodeJS podría tener la misma situación de conflicto de versiones entre ambientes, por lo que que usaremos también un gestion de versiones de NodeJS para lidiar con estos escenarios.

**NOTA** si ya tienes instalado NodeJS u otro motor de javascript compatible con el interprete de javascript V8 o similar, no deberás hacer este paso, aunque es ampliamente recomendable usar un gestor de versiones, si lo quieres llegar a usar debes desinstalar el intérprete de javascript que tengas instalado para evitar conflictos futuros.

Para gestionar la versiones de NodeJS usaremos NODENV ejecutando la siguiente secuencia de comandos, que instalará el NODENV, añadirá sus binarios a la variable de entorno PATH, e instalará la versión 12.17.0 de NodeJS.

```sh
$ curl -fsSL https://raw.githubusercontent.com/nodenv/nodenv-installer/master/bin/nodenv-installer | bash
$ echo 'export PATH="$HOME/.nodenv/bin:$PATH"' >> ~/.bashrc
$ echo 'eval "$(nodenv init -)"' >> ~/.bashrc
$ source ~/.bashrc
$ nodenv install 12.17.0
$ nodenv global 12.17.0
```

### Instalando el ambiente de desarrollo con Rails y PostgreSQL

Habiendo instalado RBENV y BUNDLER ya podemos empezar a instalar todas las gemas, herramientas y servidores necesarios para poder seguir adelante con nuestros primeros pasos con Ruby on Rails

Vamos a instalar Ruby on Rails usando el siguiente comando (si, Rails es también una gema).

```sh
$ gem install rails
```

Rails instalará con varias gemas como dependencias para su ejecución, así que obtendremos una instalación con una larga lista de librerías, al final podrás corroborar la versión de Rails usando el comando de abajo, que en nuestro caso será la versión `6.0.3.2`

```sh
$ rails -v
#=> Rails 6.0.3.2
```

Las versiones de Rails superiores a la 6, requieren usar YARN como gestor de paquetes de javascript y habilitar el enlace de Webpack usando la tecnología Webpacker, para esto, vamos a usar la versión oficial de los repositorios de YARN a través de los siguientes comandos

```sh
$ sudo curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

$ sudo sh -c "echo 'deb https://dl.yarnpkg.com/debian/ stable main' >> /etc/apt/sources.list"

$ sudo apt update
```

Para instalar YARN vamos a usar el comando de abajo, y usaremos nuestra versión de NodeJS instalada en el paso anterior usando la opción `--no-install-recommends`

```sh
$ sudo apt --no-install-recommends install yarn
```

Finalmente, vamos a instalar la base de datos de postgreSQL descargando el ejecutable desde esta pagina https://www.postgresql.org/download/windows/ recuerda el usuario y la contraseña que asignes en el proceso de instalación, dado que lo necesitaremos para la configuración de Rails.

### Accediendo a mi código

Para usar el sistema de directorios de windows dentro del ambiente de Linux, debes utilizar la siguiente instrucción que accederá a la raíz de la unidad **C** y a partir de allí ejecutar tus comandos

```sh
$ cd /mnt/c
```

Alli podras crear directorios de trabajo que podrás acceder desde cualquier editor de texto en tu entorno windows, para nuestro curso haremos el directorio `coding/platzi`

```sh
$ mkdir -p coding/platzi
```

# 3. Nuestra primera aplicación

## Entender la web con rieles

Todo parte del esquema solicitante-proveedor. O sea, cliente-servidor. Rails no es la excepción a esto y podemos encontrar este tipo de flujos utilizando arquitecturas modernas.

<h3>1er paso - Cliente</h3>

- Protocolo HTTP
- Petición URL
- Método GET
- Formato HTML

<h3>2do paso - Enrutador/Mapeador</h3>

- Petición URL
- Método GET

<h3>3er paso - Controlador</h3>

- Formato HTML
- Parámetros :id

<h3>4er paso - Modelo</h3>

- Parámetros :id

<h3>5er paso - Templating, Formatos y Renderizador</h3>

- Formato HTML
  Cuando ya tenemos los datos que hemos pedido adoptamos el formato que viene desde nuestra petición y lo renderiza con un sistema de plantillas

<h3>6to paso - Formato</h3>

- Formato HTML
  Página lista.

------

Esto denota un estilo arquitectónico o **MVC** (Modelo-vista-controlador)

⭐ Rails trata por convención a las peticiones con la estructura RESTful, este nos provee cierto tipo de métodos prediseñados y preconfigurados para poder acceder a los recursos.

#### Web tradicional

La gran mayoría de sitios y aplicaciones en internet trabajan con un arquitectura Cliente - Servidor. Incluso en desarrollos con tecnologías modernas.

#### Web con Rails

El cliente hace una petición, el enrutador hace el papel de una especie de lobby, donde dirigirá la petición a donde está lo que pidió, pero antes se topa con el controlador; el controlador construye la respuesta y la envía (con ayuda del modelo y del renderizador) de regreso.

1. Para realizar esa petición se usa el protocolo HTTP, con una petición URL usando el método GET en formato HTML.
2. La petición URL llega al sistema, y el enrutador hace un mapeo y a través del mapeo se enruta hacia su correspondiente controlador.
3. El controlador reconoce los parámetros que vienen dentro de la URL y con está información se comunica con el Modelo para obtener la información que se pidió.
4. Cuando ya se tiene la información adopta el formato que fue solicitado y lo renderiza con un sistema de plantillas y finalmente devuelve el resultado al cliente.

Este es un estilo arquitectónico conocido como MVC (Model View Controller)

Por convención, Rails trata a las peticiones bajo el estilo arquitectónico RESTful, que provee ciertos métodos prediseñados y preconfigurados para acceder a los recursos. Estos son: index, show, new, create, update y destroy.

Estos métodos se pueden modificar o incluso se pueden crear métodos propios, pero estos son los que la arquitectura RESTful provee por defecto.

**Muy importante:**

- Index
- Show
- New
- Edit
- Create
- Update
- Destroy

[03 | Rails Fundamental | El MVC](https://www.youtube.com/watch?v=Izkaq1rjukg)

[Docker-compose | rails | postgresql | redis](https://github.com/supermavster/docker-ruby-on-rails)

## Primero pasos con Ruby on Rails

Crea la primer app

```sh
rails new my_app -T
```

Base datos

```sh
rails new myapp -d postgresql
```

Para iniciar un proyecto con rails usamos el comando:

```bash
rails new *nombre_del_proyecto*
```

Si queremos ver los tipos de comandos que podemos crear con rails escribimos:

```bash
rails new -h
```

Es bueno que para este proyecto especifiques la DB que vas a usar, el comando que vas ejecutar sería este:

```bash
rails new *nombre_del_proyecto* -T -d postgresql
```

📕 Las carpetas con las que más vamos interactuar será:

- app
- config

## Entender el enrutamiento básico

Vamos crear una ruta con ruby.

El método ***get\*** en RoR nos hace un sistema de mapeo que conseguirá el método GET, y este va a buscar un $PATH llamado ‘hola’, ahora a partir de ese mapeo va utilizar una equivalencia para enviarlo a un controlador y a una acción dentro del controlador.

```ruby
# routes.rb
Rails.application.routes.draw do
  # En este caso el PATH es 'hola', el controlador es 'welcome' y su acción es 'hello'
  get 'hola', to: 'welcome#hello'
end
```

> ♦ Rails utiliza la convención de que toda la lógica y construcción que vamos a hacer sea utilizando el leguaje Inglés.

Dentro de app → controllers vamos a crear los archivos de los controladores, para hacerlo tenemos que poner el nombre que tiene el controlador que definimos en la ruta y añadir al final ***_controller.rb\***

> ♦ Para crear clases en Ruby debemos utilizar la convención de un sistema CamelCase.

Para convertir una clase en un controlador debemos utilizar una herencia de RoR que es llamada ***ApplicationController.\*** Esta clase de ***ApplicationController\*** dotará una serie de funcionalidades y métodos que nos permitirán establecer nuestra clase **WelcomeController** como un controlador de RoR.

```ruby
# welcome_controller.rb

class WelcomeController < ApplicationController
    # Este método 'hello' es una acción en Rails
    def hello
        @var = Pet.first.name
        @var2 = Pet.first.breed

        @credentials = Rails.application.credentials.hello
    end
end
```

Para crear nuestra vista debemos ir a app → views y crear el directorio con el nombre del controlador y un archivo html llamado como la acción.

```ruby
# hello.html
```

Para prender el servidor de rails, escribimos el comando:

```bash
rails server
```

> ♦ Para acceder a la ruta escribimos localhost:3000/hola

[gemas](https://www.youtube.com/watch?v=Sx8KSWL9J6Y)

## Manipular el patrón MVC

Para generar o abordar cierto tipo de información de forma rápida, escribimos el comando:

```bash
rails g *model Nombre atributo1:string atributo2:string*
```

💎 El comando ***rails g\*** nos permitirá generar estructuras prediseñadas y le vamos a pasar qué tipo estructura queremos generar y el nombre de la estructura. Luego vamos a introducir los atributos junto a su tipo de dato.

Cuando ejecutemos este comando se van a generar dos archivos, uno es un archivo de la tabla que creamos para la DB y el otro es del modelo.

Este es el archivo que creo en el modelo, como podemos ver está heredando de una clase que se llama ***ApplicationRecord\***, la cual se encarga de indicarle a Rails que la clase hija se trata de un modelo.

```ruby
# pet.rb
class Pet < ApplicationRecord
end
```

En la carpeta db → migrate se encuentra el archivo que contiene la tabla que creamos para la base de datos.

```ruby
# 7274580843_create_pets.rb

class CreatePets < ActiveRecord::Migration[6.0]
  def change
    create_table :pets do |t|
      t.string :name
      t.string :breed

      t.timestamps
    end
  end
end
```

Para pasar información desde Rails a las vistas, para esto debemos cambiar la extensión de nuestro archivo html a uno que le indique que dentro se va renderizar código Ruby.

```ruby
# hello.html.erb
```

Si quieres añadir el contenido de una variable de ruby debemos utilizar:

```ruby
Hola <%= @nombre %>
```

Recuerda que después de generar el modelo debemos escribir este comando para que termine el proceso.

```bash
rails db:migrate
```

Con el siguiente comando vamos a poder entrar a un contexto de interacción dinámica con la cual podemos crear una instancia nueva y que está se guarde en la DB.

```bash
rails console
```

Para introducir un nuevo registro en la DB vamos a poner lo siguiente:

```bash
Pet.create name: 'Arnold', bread: 'poodle'
```

![code.png](https://static.platzi.com/media/user_upload/code-555bffff-022f-465e-8691-8efb7eb3149b.jpg)

![code1.png](https://static.platzi.com/media/user_upload/code1-0fd723ad-0266-436f-b667-f060ec4edf34.jpg)

## Los secretos de Rails

Ahora Rails 6 podemos agregar credenciales para múltiples entornos y en cada uno se encontraran sus propias claves de cifrado.
_
**Credenciales globales**

Para cualquier entorno, Rails detecta automáticamente qué conjunto de credenciales utilizar. Las credenciales específicas del entorno tendrán prioridad sobre las credenciales globales. De no encontrarse credenciales específicas del entorno Rails utilizará las credenciales globales y la clave maestra que están representadas por los siguientes archivos:

- `config/credentials.yml.enc`
- `config/master.key` (Archivo a compartir con nuestro equipo)
  _
  **Cree credenciales espesifico (producción)**

Ejecutaremos el siguiente comando:

```ruby
 EDITOR=nano rails credentials:edit --environment production
```

Este comando abrirá el editor de texto seleccionado y nos permitirá crear las credenciales (formato, clave - valor) que consideremos necesarios, ademas nos creara los siguientes archivos

- `config/credentials/production.key` (Archivo a compartir con el equipo que tenga acceso al entorno producción)
- `config/credentials/production.yml.enc`

_
**Usando las credenciales en Rails**

Para desarrollo:

```ruby
rails c
Rails.application.credentials.config # nos mostrara todo el contenido
Rails.application.credentials.aws # nos mostrara el valor de la clave aws
```

Para producción:

```ruby
RAILS_ENV=production rails c
Rails.application.credentials.config # nos mostrara todo el contenido
Rails.application.credentials.aws # nos mostrara el valor de la clave aws
Conceptos extraídos de:
```

- https://blog.saeloun.com/2019/10/10/rails-6-adds-support-for-multi-environment-credentials.html

En el controller welcome_controller.rb

```ruby
def hello
    @variable = Pet.first.name
    @variable_dos = Pet.first.breed
  end
```

En el hello.html.erb:

```ruby
<p>Hola me llamo <%= @variable %> y soy un <%= @variable_dos %></p>
```

**Como trata Rails los datos sensibles**

Una base de datos viene por defecto en las aplicaciones RoR llamada SQLite. SQLite tiene integrado, por defecto, un sistema de credenciales de acceso.
Manejo de secretos con Rails
++
Rails ha diseñado estrategias para lidiar con datos confidenciales. Desde sus primeras versiones hasta la actualidad se ha usado el archivo database.yml. En este archivo esta la configuración de acceso para las bases de datos.
++
No fue hasta la versión 5.2 en que se integraron credenciales encriptadas. Gracias a esta actualización se obtuvo la posibilidad de compartir estructuras mientras se conservaba la confidencialidad.
++
En el archivo database.yml se centra todo le sistema de información sensible.
++
El archivo credentials.yml.enc es un archivo con contenido encriptado. No es posible editarlo ya que necesita del cmd. En el cmd escribimos

```sh
EDITOR=nano rails credentials:edit
```

Este comando nos llevará a un editor en consola donde podremos editar el contenido. Una vez ahi podemos ver una clave por default. Podemos crear la nuestra usando una palabra:numero (hello:123456). Usamos ctrl + x -> y - > enter. Podemos acceder a este archivo usando la clase
Rails. Podemos acceder a las claves creadas usando .nombre_clave

```sh
Rails.application.credentials
```

Si vamos al controlador, creado previamente en el editor de texto (welcome_controller.rb), podemos crear una variable dentro de la acción que tenemos (hello), a esta variable le asignamos la invocación anterior (Rails.application.credentials)

[![img](https://www.google.com/s2/favicons?domain=https://12factor.net/images/favicon.ico)The Twelve-Factor App](https://12factor.net/)

## Assets y Layouts

Optimización: para que los assets no tomen tanto tiempo a la hora de ser cargados en la pagina web.

Caching: Evidenciar patrones de pre carga, donde no vas a tener que pedir siempre la misma información.

Ofuscacion: cargar un sistema en donde sea seguros los assets y se pueda comprimir.

Seguridad: utilizar fingerprints que permite acceder a los assets evitando ambiguedades entre ellos.

Son insumos que las respuestas HTML (y otras) necesitan para operar funcional y estéticamente de forma correcta en el cliente. Archivos CSS, JS, imágenes, fuentes, etc.

Cargar estos assets en una aplicación web puede llegar a ser un problema si no se manejan de forma correcta.

Se necesita un sistema de optimización, para que no tarde tanto el cargar los assets, también utilizar sistemas de caching para tener cierta información precargada, otra técnica es la ofuscación, para comprimir y cifrar los assets y por último sistemas de seguridad. Todo esto se puede gestionar desde Rails.

El sistema de assets en Rails se base en el Asset Pipeline

### Asset Pipelin

El Asset Pipeline se puede pensar como una banda que va llevando a los assets por ciertas fases para llegar al paquete final que se enviará a la web.

#### Transformer

En esta etapa se transforman los archivos a los formatos que soporta la web, es decir. Por ejemplo, si se trabaja con SASS estos archivos pasarán por un transformador para convertirlos en CSS. Esto con todos los dialectos o meta lenguajes con los que se este trabajando

#### Concatenator

Lo que sucede aquí es que, por ejemplo, todos los archivos CSS se unan en un solo archivo y esto permite que solo se haga una petición para enviar todos los estilos de la aplicación.

#### Compressor

Lo que el compresor es minificar los archivos que obtuvo del Concatenator para que sean lo más ligeros posible.

#### Fingerprinter

El fingerprinter le da un ID único al paquete para indicarle al sistema de caching las versiones de assets y así sepa cuando es necesario reemplazar la información en caché.

------

### Layouts

Por lo general, las vistas de las aplicaciones web están divididas en secciones, como una barra de navegación, la sección del contenido principal y el footer, esto es un layout.

El Asset Pipeline se enlaza al layout al principio, en algo llamado Sporckets manifest. Aquí se referencia la estructura del Asset Pipeline y se empiezan a agrupar los assets necesarios para esa página en específico.

Si la página se vuelve muy compleja se puede dividir en parciales para que no se tan complicado manejarla.

Con ayuda de el método yield, proporcionado por Rails, se manejará el contenido principal de las plantillas, para que solo este cambie y el resto de la página siga como en la plantilla.

------

### Webpacker

Webpacker, recientemente añadido a Rails, es un componente que utiliza internamente Webpack. Con el propósito de tener un desarrollo de aplicaciones tanto tradicional como moderno.

Así como para gestionar las librerías de Ruby se utiliza el Gemfile, para el JS de la aplicación se utiliza YARN. Con YARN se gestionarán todas las librerías de JavaScript, asumiendo que los archivos JS también se pueden ver como assets.

[oficial del assetpipeline](https://guides.rubyonrails.org/asset_pipeline.html)

[![img](https://www.google.com/s2/favicons?domain=https://guides.rubyonrails.org/asset_pipeline.htmlimages/favicon.ico)The Asset Pipeline — Ruby on Rails Guideshttps://guides.rubyonrails.org/asset_pipeline.html](https://guides.rubyonrails.org/asset_pipeline.html)

## Agregar el primer conjunto de scaffolds

**¿QUÉ ES SCAFFOLD?**
Un conjunto de archivos generados automáticamente que forman la estructura básica de un proyecto de Rails. Permite crear una abstracción de un elemento real. Estos incluyen:

- Un controlador
- Un modelo
- Vistas para cada acción estándar del controlador (Index, edit, show, new, destroy)
- Una ruta
- Una migración para la base de datos

**COMO USAR SCAFFOLD**
Se necesita escribir rails g scaffold, el recurso y sus _atributos _ incluyendo el tipo de dato usando dos puntos ( : )

```sh
 rails g scaffold Book title:string author:string description:text isbn:string pages_count:integer
```

command

```sh
rails g scaffold _recurso_ atributos:string
```

Luego de usar el comando se generará una serie de archivos. Todo lo que diga invoke hará referencia a un invocador. Algunos son:

- active_record: Crea dos archivos para las migraciones
- resource_route: Crea las rutas de nuestro elemento. Esto incluye controladores y acciones (ndex, edit, show, new, destroy)
- scaffold_controller: Crea un controlador de nuestro recurso
- helper: Crea abstracciones dentro de las vistas
- jbuilder: Permite crear archivos de tipo JSON
- assets / scss : Crea un archivo tipo SASS en la carpeta asstes para modificar estilos.

**ACCEDER A NUESTRO RECURSO**
Para acceder al recurso, asi como sus acciones debemos iniciar el servidor usando rails s. Una vez activado vamos al navegador y colocamos la url 127.0.0.1:3000/elemento.

- Podemos ir a routes.rb para ver la configuración de esta acción.
- Para ir directo a la acción del controlador del recurso usamos recurso_controller.rb. Aqui podemos ver varias funciónes correspondientes con cada vista (index, show, new, edit, destroy).
- Para ver vista vamos a la carpeta Views.

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Ruby on Rails Guides: Rails Routing from the Outside In](https://guides.rubyonrails.org/v3.2/routing.html#resources-on-the-web)

## Cómo funcionan las migraciones

**Componentes de las migraciones de base de datos:**

- *db/migrate* directorio que lleva los procesos donde se hacen las modificaciones entre modelos y tablas. Sus archivos se llamarán Migration 1, 2, N … Y estos permiten crear, editar o eliminar atributos y columnas de la base de datos. Estos procesos se reflejan en el schema.
- *Schema* condensa las estructuras de los modelos de las tablas de la base de datos. Aqui se ve la equivalencia entre la base de datos y los modelos.
  En el archivo *db/migrate* podemos ver las migraciones que hemos creado. Si vamos a uno de estos archivos podemos ver: El nombre del modelo *:pets* (que sería la tabla) y sus atributos *:name* y *:breed* (que serian las columnas)

```ruby
class CreatePets < ActiveRecord::Migration[6.1]
  def change
    # Por buena práctica el modelo tiene nombre en plural
    create_table :pets do |t|
      t.string :name
      t.string :breed

      t.timestamps
    end
  end
end
```

En el archivo *db/schema* podemos ver la versión de nuestra base de datos, referenciada con la base de datos. Tambien podemos ver la estructura de nuestras migraciones.

Para ver el estado de nuestras migraciones usamos el comando *rails db:migrate:status* con esto podremos ver el nombre de la migración, su ID, y su estado.

Una migración consiste en una serie de instrucciones escritas en Ruby para modificar el esquema de la base de datos.

Para ejecutar las migraciones pendientes:

```sh
$ rails db:migrate
```

Para revertir la ultima migración:

```sh
$ rails db:rollback
```

Para conocer el estado de las migraciones:

```sh
$ rails db:migrate:stat
```

## Optimiza tu código con HAML

> Pero como HAML tiene bajo rendimiento, necesitaré instalar HAMLIT. Así finalmente podré tener como resultado código HTML. Genial! XD

**HAML**

- Es un elemento de optimización.
- Es un elemento de plantillas que reemplazará a erb (elemento que pasa de codigo ruby a render).
- Este componente mejora la legibilidad y velocidad para escribir código.
- Su uso es totalmente opcional
  Para instalarlo solo es necesario usar la gema haml:

```
gem install haml
```

Existen comentarios que el uso de haml puede llegar a afectar el rendimiento de nuestro proyecto. Para prevenir esto podemos usar hamlit.
**HAMLIT**
Gema que codensa información de renderización para que gaste menos recursos.
Para instalarlo es necesario ir a nuestro gemfile. Casi al final de este archivo (antes de los groups) colocaremos la gema:

```sh
gem 'hamlit'
```

Para establecer el enlace entre hamlit y Rails usamos hamlit-rails. Lo colocaremos en el grupo :development

```ruby
group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 4.1.0'
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'hamlit-rails'
end
```

Para instalar la gema colocaremos el siguiente comando en la consola:

```sh
bundle install
```

Este comando detectará las nuevas librerias para comenzar a instalarlas.
Para comenzar a usarl haml solo hace fatla cambiar la extensión .html.erb por .html.haml

---

HAML es un sistema que reemplazará a el sistema erb. Este sistema mejora la legibilidad y la velocidad con la que se escribe código. Cabe mencionar que el uso de este sistema de plantillas es opcional.

Para usar este sistema primero hay que instalar la gema llamada haml. Es recomendable utilizar hamlit en lugar de haml, ya que mejora su rendimiento y consumo de recursos.

Otra gema que se recomienda utilizar al desarrollar con Rails es hamlit-rails, que ayuda a establecer una conexión entre hamlit y Rails.

Para instalar las gemas las agregamos en el archivo Gemfile y luego ejecutamos el comando `bundle install`.

Para cambiar el sistema de plantillas hay que cambiar la extensión del archivo de erb a haml, y después rehacer el código en haml, ya que es diferente al código que se usa en erb.

> HAML es genial, ayuda bastante cuando estas en las vistas, quedan mas claras y ahorras bastante tiempo. Excelente curso, es muy claro y bien enfocado.

[![img](https://www.google.com/s2/favicons?domain=http://haml.info//images/favicon.ico)Haml](http://haml.info/)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - haml/haml: HTML Abstraction Markup Language - A Markup Haiku](https://github.com/haml/haml)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - k0kubun/hamlit: High Performance Haml Implementation](https://github.com/k0kubun/hamlit)

## Agiliza la construcción de formularios con Simple Form

**¿Qué es?**

- Componente que agiliza la construcción de los formularios
- Es una Gema

**Instalación:**

1. Añadir: *gem ‘simple_form’* en el gemfile.
2. Usar el comando *bundle install* (o *bi*)
3. Correr el generador usando: *rails generate simple_form:install* en el CMD
   Este proceso generará una preconfiguración del simple form usando plantillas.

**Generar un form**

- Generaremos un scaffold para el modelo User como hemos visto anteriormente. En este caso usando los atributos first_name, last_name, address, about_me y phone.
- Podemos ver que ahora, en las invocaciones, en lugar de erb, llamará a HAML
- Es posible añadirle cosas extra a los atributos creados. Por ejemplo, un label, que cambiará el nombre que se muestra en el form de la página web. Más de esto se puede ver en la documentación de simple form.

Es posible hacer la conversión de HTML a HAML usando la herramienta [HTML2HAML](https://html2haml.com/)

> Simple Form es una dependencia que ayuda con la creación de formulario manteniendo la estructura MVC.
>
> Para instalar la gema hay que añadir `gem 'simple-form'` a la lista de dependencias en el Gemfile y después ejecutar el comando `bundle install`
>
> Una vez instalado hay que ejecutar el comando `rails g simple_form:install` para que la dependencia se configure con una serie de plantillas y un inicializador.
>
> Este comando crea tres archivos: el inicializador, que es una pre configuración de la librería; el archivo de internacionalización, y la plantilla del formulario que para generar los scaffolds utilizando haml.
>
> ------
>
> Al crear un nuevo scaffold se aprecia que para la creación de las vistas ya no utilizó erb, sino haml.
>
> Al comparar las plantillas generadas con haml con las previamente generadas con erb, se observa que las de haml tienen una estructura mucho más limpia y fácil de leer con muchas menos líneas de código.
>
> Existen conversores de HAML a HTML, en caso de que usar la nueva sintaxis se dificulte.

el partial de _form de libros lo podríamos colocar en haml de la siguiente manera:

```ruby
-# frozen_string_literal: true
= simple_form_for(@book) do |form|
  -if book.errors.any?
    #error_explanation
      %h2
        = pluralize(book.errors.count, "error")
        prohibited this book from being saved:
      %ul
        - book.errors.full_messages.each do |message|
          %li= message
  .field
    = form.label :title
    = form.text_field :title
  .field
    = form.label :author
    = form.text_field :author
  .field
    = form.label :description
    = form.text_area :description
  .field
    = form.label :isbn
    = form.text_field :isbn
  .field
    = form.label :pages_count
    = form.number_field :pages_count
  .actions
    = form.submit
```

bloque de inputs transformando esta parte

```ruby
.field
  = form.label :description
  = form.text_field :description
```

a esto

```ruby
= form.input :description, label: 'description'
```

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - heartcombo/simple_form: Forms made easy for Rails! It's tied to a simple DSL, with no opinion on markup.](https://github.com/heartcombo/simple_form)

[![img](https://www.google.com/s2/favicons?domain=https://htmltohaml.com//assets/logo.png)Convert HTML to HAML](https://htmltohaml.com/)

## Soporte de varios idiomas para tu aplicación

**Internacionalización**
El sistema de internacionalización o I18N nos ayuda a interactuar con una libreria de Rails para insertar de forma automatizada ciertas keys para enlazar traducciones a diferentes idiomas.

**Cómo usarlo**

- En los labels de los atributos del modelo User escribiremor t(‘model.atribute’). Esto, de forma automática accederá al atributo del modelo.
- Buscamos el archivo config/locales/en.yml Este archivo contendrá “etiquetas”. Colocamos el nombre del modelo y sus atributos. Dentro de estas etiquetas colocaremos su traducción.

```ruby
en:
  hello: Hello world
  users:
    first_name: First name
```

- El archivo “en” funciona como diccionario del idioma ingles. Para crear uno del idioma español podemos duplicarlo y llamarlo “en”. En el archivo en colocaremos los términos en español.
- Para ahorrarnos el proceso de escribir cada atributo de forma manual podemos usar el comando i18n-asks add-missing. Esto requiere una unstalacion (gem install i18n-tasks).

**Establecer lenguaje por acciones**

- Vamos al controlador que queremos internacionalizar podemos añadir la clase I18n y el método locale, para establecer el lenguaje de la acción. ‘es’.

```ruby
def new
	I18n.locale = 'es'
	@user = User.new
end
```


**Establecer lenguaje por controlador**

- Si queremos que esta internacionalización afecte a todas las acciones tendríamos que ir a su controlador base. Dentro de este colocaremos dentro de la clase:

```ruby
before_Action :set_locale

def set_locale
	I18n.locale = 'es'
end
```

**Internacionalización**
El sistema de internacionalización o I18N nos ayuda a interactuar con una libreria de Rails para insertar de forma automatizada ciertas keys para enlazar traducciones a diferentes idiomas.

**Cómo usarlo**

- En los labels de los atributos del modelo User escribiremor t(‘model.atribute’). Esto, de forma automática accederá al atributo del modelo.
- Buscamos el archivo config/locales/en.yml Este archivo contendrá “etiquetas”. Colocamos el nombre del modelo y sus atributos. Dentro de estas etiquetas colocaremos su traducción.

```ruby
en:
  hello: Hello world
  users:
    first_name: First name
```

- El archivo “en” funciona como diccionario del idioma ingles. Para crear uno del idioma español podemos duplicarlo y llamarlo “en”. En el archivo en colocaremos los términos en español.
- Para ahorrarnos el proceso de escribir cada atributo de forma manual podemos usar el comando i18n-asks add-missing. Esto requiere una unstalacion (gem install i18n-tasks).

**Establecer lenguaje por acciones**

- Vamos al controlador que queremos internacionalizar podemos añadir la clase I18n y el método locale, para establecer el lenguaje de la acción. ‘es’.

```ruby
def new
	I18n.locale = 'es'
	@user = User.new
end
```

**Establecer lenguaje por controlador**

- Si queremos que esta internacionalización afecte a todas las acciones tendríamos que ir a su controlador base. Dentro de este colocaremos dentro de la clase:

```ruby
before_Action :set_locale

def set_locale
	I18n.locale = 'es'
end
```

es.yml 👀

```yml
---
es:
  hello: Hola mundo
  users:
    edit:
      back: Regresar
      show: Mostrar
    first_name: Nombres
    form:
      about_me: Un poco sobre mi persona
      address: Direccion
      last_name: Apellidos
      phone: Telefono
    index:
      about_me: Un poco sobre mi
      address: Direccion
      are_you_sure?: ¿Estas seguro?
      destroy: Destruir
      edit: Editar
      first_name: Nombres
      last_name: Apellidos
      listing_users: Lista de usuarios
      new_user: Nuevo usuario
      phone: Telefono
      show: Mostrar
    new:
      back: Regresar
    show:
      about_me: Sobre mi persona
      address: Direccion
      back: Regresar
      edit: Editar
      first_name: Nombres
      last_name: Apellidos
      phone: Telefono```
```

```ruby
.form-inputs
    = f.input :first_name, label: t('users.first_name')
    = f.input :last_name, label: t('.last_name')
    = f.input :about_me, label: t('.about_me')
    = f.input :phone, label: t('.phone')
```

Books:

```ruby
.form-inputs
    = f.input :title, label: t('.title')
    = f.input :author, label: t('.author')
    = f.input :description, label: t('.description')
    = f.input :isbn, label: t('.isbn')
    = f.input :pages_count,label: t('.pages_count')
```

‘es’ file:

```ruby
es:
  books:
    form:
      author: Autor
      description: Descripción
      isbn: Isbn
      pages_count: Número de páginas
      submit: Registrar
      title: Titulo
  hello: Hello world
  users:
    first_name: Nombres
    form:
      about_me: Sobre mi
      last_name: Apellidos
      phone: Telefono
      submit: Registrar
```

[![img](https://www.google.com/s2/favicons?domain=https://rubygems.org/gems/i18n-tasks/versions/0.9.5/apple-touch-icons/apple-touch-icon.png)i18n-tasks | RubyGems.org | your community gem host](https://rubygems.org/gems/i18n-tasks/versions/0.9.5)

## Debugging: detecta los errores en tu código

Rails instala por defecto un librería llamada byebug, esta librería es debugger por defecto que utiliza Ruby y Rails.

Para utilizarlo hay que colocar la palabra `byebug` en la parte del código en la que se quiera parar la grabación, esto es útil para interactuar con las variables, constantes o parámetros en ese momento y ver en que momento está fallando el código.

Para ver estos valores hay que ir a la terminal donde se está ejecutando el servidor, y se podrá observar dónde se paró la ejecución de la aplicación, y también podrás ver el valor de todas las variables, clases, métodos, etc. solo con poner el nombre y dar enter en la terminal.

Para seguir el flujo del código se ejecuta el comando `continue`, y esto terminará la ejecución del código.

------

Otra herramienta de debugging bastante conocida es pry, está gema es una librería de debugging para Ruby y Rails y es algo más madura que byebug.

Para utilizarla hay que instalar las gemas hay que agregar pry y pry-doc en el Gemfile y luego ejecutar el comando `bundle install`.

Una vez instalada, para comenzar a hacer debugging se utiliza `binding.pry` en el lugar donde se quiera parar la ejecución de la aplicación y de la misma forma en la terminal donde se está ejecutando el servidor podremos interactuar con las variables y métodos.

una correcta forma de usar ese debugger es a través de este código en el gemfile

```ruby
group :development, :testdo
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'pry-rails'
  gem 'ruby_jard'
end
```

el cual es muy similar a lo que tienes, no he probado su funcionamiento con entornos windows, pero te diría que probaras de la forma más simple, primero con un proyecto en ruby puro, y luego con un proyecto de rails, sólo dejando el ruby jard (igual este internamente usa byebug y pry)

```ruby
group :development, :testdo
  gem 'ruby_jard'
end
```

[![img](https://www.google.com/s2/favicons?domain=https://rubygems.org/gems/pry/versions/0.10.3/apple-touch-icons/apple-touch-icon.png)pry | RubyGems.org | your community gem host](https://rubygems.org/gems/pry/versions/0.10.3)

[![img](https://www.google.com/s2/favicons?domain=https://rubygems.org/gems/pry-doc/versions/0.8.0/apple-touch-icons/apple-touch-icon.png)pry-doc | RubyGems.org | your community gem host](https://rubygems.org/gems/pry-doc/versions/0.8.0)

[Ruby JArd](https://rubyjard.org/)

# 4. Proyecto del curso: primeros pasos

## ¿Qué vamos a desarrollar?

**Iniciamos nuevo proyecto con postgresql**

```sh
rails new organizaodr -T --database=postgresql  
```

Instalamos las dependencias con yarn

```sh
yarn add boostrap jquery popper.js roboto-fontface
```

**Agregamos las siguientes gemas**

```ruby
gem "hamlit"
gem "simple_form"
# group :development, :test do
  gem "pry"
  gem "pry-doc"
# group :development do 
gem "hamlit-rails"
```

instalamos

```sh
bundle install
bi
```



>  **yarn** es un gestor de paquetes que te permite organizar de forma consistente las librerias de los assets que en este caso usa el webpacker, habiendo dicho esto, cada librería es un mundo pequeño donde se resuelve una responsabilidad, para el caso de esta libreria roboto-fontface, esta almacena las fuentes localmente en el ya conocido directorio **node_modules** por [aquí](https://github.com/choffmeister/roboto-fontface-bower) puedes encontrar más info de esta librería, por otro lado, no todas las librerías de este estilo tienen la misma forma de almacenar los assets, por lo que lo mejor es leer la documentación antes de usar una de ellas.

shortcut para la bandera de la base de datos, quedaría así:

```sh
rails new organizacion -T -d postgresql
```

Yarn en docker:

```yaml
FROM ruby:2.7.2-buster

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
```

## Diseñando el modelo de datos

Diseño de la aplicación y cambios en la misma para recomendarles un libro que estoy leyendo actualmente que me ha ayudado mucho a mejorar la forma en que escribo código, el libro se llama Practical Object Oriented Design. Este libro incluye ejemplos muy fáciles de enteder pero de alto impacto para el futuro de como diseñamos el implementamos. La autora tiene mucha experiencia con el diseño orientado a objetos y lo mejor es que el libro tiene todos sus ejemplos en Ruby. Espero que todxs le saquen mucho provecho 😄 **[Chéquenlo aquí](https://www.poodr.com/)**

> una relación *muchos-a-muchos* necesitas tradicionalmente una **tabla intermedia** (que Rails puede gestionar de forma transparente, en esta clase no lo hacemos así), sin embargo, **Participant** es mas bien una *associative entity* (una tabla intermedia con esteroides con la que podré tener referencias a otras tablas mediante un registro) que me permitirá tener un *associative relationship attribute* es decir un atributo que sólo tiene sentido en la asociación **Task-User**, estoy hablando del atributo **role**, este atributo no podría ir ni en la tabla **Task**, ni en la tabla **User**

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Domain-Driven Design: Tackling Complexity in the Heart of Software: Evans, Eric: 8601300201665: Amazon.com: Books](https://www.amazon.com/Domain-Driven-Design-Tackling-Complexity-Software/dp/0321125215)

## Construye los primeros scaffolds del proyecto

```sh
❯ rails g simple_form:install --bootstrap
```

#### crear scaffolds 

##### | Category

```sh
❯ rails g scaffold Category name:string description:text
```

##### | Task

```sh
rails g scaffold Task name:string description:text due_date:date category:references
```

### gem annotate

```sh
annotate --models
```

Al momento de generarlas, no, es decir al momento de generar el archivo. Al momento de ejecutarlas, sí. Una vez que tienes bien la configuración de la base de datos en `database.yml` puedes pedirle a rails que la cree por ti:

```ruby
railsdb:create
```

Y posteriormente:

```ruby
railsdb:migrate
```

Como dato extra, puedes hacer ambos pasos con:

```ruby
rails db:setup
```

Si quieres tener más información sobre qué puedes hacer con el comando rails, puedes ejecutar el siguiente comando para obtener una lista:

```ruby
rails -T
```

Error al generar la migracion, o la migracion existen. con el siguiente comando corriges el error - Siempre que tu base de datos este vacio o nueva.

```sh
rake db:drop db:create db:migrate
```

[![img](https://www.google.com/s2/favicons?domain=https://rubygems.org/gems/annotate/versions/2.7.1/apple-touch-icons/apple-touch-icon.png)annotate | RubyGems.org | your community gem host](https://rubygems.org/gems/annotate/versions/2.7.1)

[Construye los primeros scaffolds del proyecto](https://platzi.com/clases/1976-intro-ror/29925-construye-los-primeros-scaffolds-del-proyecto/)

## Internacionalizando los modelos

new.html.haml file

```ruby
%h1= t('tasks.tasks')

= render 'form'

= link_to t('common.back'), tasks_path
```

_form.html.haml file

```ruby
-# frozen_string_literal: true
= simple_form_for(@task) do |f|
  = f.error_notification
  = f.error_notification message: f.object.errors[:base].to_sentence if f.object.errors[:base].present?

  .form-inputs
    = f.input :name, label: t('.name')
    = f.input :description, label: t('.description')
    = f.input :due_date, label: t('.due_date')
    = f.association :category, label: t('.category')

  .form-actions
    = f.button :submit
```

es.yml file

```ruby
---
es:
  activerecord:
    models:
      category:
        one: Categoría
        others: Categorías
      task:
        one: Tarea
        others: Tareas
  categories:
    form:
      description: Descripción
      name: Nombre
    new_category: Nueva Categoría
  common:
    back: Atrás
  hello: Hello world
  tasks:
    form:
      category: Categoría
      description: Descripción
      due_date: Fecha de Vencimiento
      name: Nombre
    tasks: Tareas
```

[Translations for Active Record Models](https://guides.rubyonrails.org/i18n.html#translations-for-active-record-models)

[![img](https://www.google.com/s2/favicons?domain=https://rubygems.org/gems/rails-i18n/apple-touch-icons/apple-touch-icon.png)rails-i18n | RubyGems.org | your community gem host](https://rubygems.org/gems/rails-i18n)

## Agregando validaciones al modelo

Para quien le interese saber más del tema de validaciones, les dejo este [excelente artículo ](https://guides.rubyonrails.org/active_record_validations.html)de las guías oficiales de [Ruby on Rails](https://api.rubyonrails.org/classes/ActiveRecord/Validations/ClassMethods.html)

Para evitar la creación de registros en blanco, o para agregar validaciones que se requieran para cumplir las reglas del negocio se pueden agregar validaciones a los campos para que lancen un error sí estas no se cumplen.

#### Validaciones

Las validaciones se agregan en el model que será validado, empezando con la palabra validates seguido de los atributos que se quiere validar. Seguido de esto se pueden usar helpers que Rails provee para validar diferentes cosas. Por ejemplo, el helper presence para validar que el campo no esté vacío.

```jsx
class Category < ApplicationRecord
    has_many :tasks

    validates :name, :description, presence: true
    validates :name, uniqueness: { case_insensitive: false }
end
```

Existen otros helpers para validar diferentes aspectos y diferentes elementos HTML. Documentación: https://guides.rubyonrails.org/active_record_validations.html#validation-helpers

#### Validación personalizada

Para validar un atributo de forma personalizada se usa la palabra validate, seguido del nombre de la función que validará el campo. La función debe ser declarada dentro de la clase.

```jsx
validate :due_date_validity

def due_date_validity   
  return if due_date.blank?
  return if due_date > Date.today
  errors.add(:due_date, I18n.t('task.errors.invalid_due_date'))
end
```

En este caso se valida que la fecha de vencimiento no esté vacía y además que la fecha no sea anterior al día actual, si no se cumplen las validaciones se agrega un error con el método errors.add.

Devise es una gema muy conocida y querida en el mundo de Rails, ya que permite generar modelos con un sistema de autenticación muy robusto y ya configurado. Repositorio: https://github.com/heartcombo/devise

Para instalar esta gema hay que agregarla al Gemfile y ejecutar el comando `bundle install`, seguido de esto hay que instalar el generador de devise con el comando `rails g devise:install`.

Ahora, para introducir usuarios al sistema es necesario crear un modelo, pero como este será un modelo con autenticación se va a generar con devise, con el comando `devise g devise User`. Una vez generado el modelo hay que ejecutar una migración de la forma habitual (`rails db:migrate`).

Devise tiene una serie de módulos que pueden ser implementados en los modelos para hacer un sistema de inicio de sesión de forma sencilla. Por defecto devise añade los módulos database_authenticable, registerable, recoverable, rememberable y validateable.

# 5. Proyecto del curso: usuarios

## Añadiendo el concepto de usuario

Gemfile

```ruby
gem 'devise'
```

instalar

```sh
bundle install
# o
bi
```

Generador de devise, todas las funcionalidades de devise.

```sh
rails generator devise:install 
# o
rails g devise:install
```

Modelo devise, no de cero. Forma Tradicional

```sh
rails g devise User
```

Migration

```sh
rails db:migrate
```

Devise es una gema super completa para el manejo de registros y sesiones de usuarios.

Si tú te estás imaginando desde ahora que puedes usar devise para iniciar sesión con Github, Twitter y otros, déjame decirte que sí se puede.

#### Rutas

En el archivo config>routes.rb divise agregó el método devise_for :users, que agregará automáticamente todas las rutas para users necesarias al sistema. Para consultar las rutas se puede usar el comando `rails routes | grep users` en la terminal, o entrar con el navegador a la ruta /rails/info/routes donde se mostrarán todas las rutas de la aplicación.

Con la ruta /users/sign_up puedes acceder a la GUI para registrar un nuevo usuario.

------

Para hacer que solo los usuarios que hayan iniciado sesión puedan acceder al sistema hay que indicar en el archivo application_controller.rb que solo los usuarios autenticados van a poder interactuar con los scaffolds creados.

Para esto se añade el método `before_action :authenticate_user!`.

#### Módulos de devise:

- database_authenticable: Crea un hash y almacena una contraseña en la base de datos para validar la autenticidad de un usuario al iniciar sesión. La autenticación se puede realizar tanto a través de solicitudes POST como de autenticación básica HTTP.
- omniauthable: Agrega soporte para OmniAuth.
- confirmable: envía correos electrónicos con instrucciones de confirmación y verifica si una cuenta ya está confirmada durante el inicio de sesión.
- cecoverable: Restablece la contraseña del usuario y envía instrucciones de reestablecimiento.
- cegisterable: Maneja el registro de usuarios a través de un proceso de registro, lo que también les permite editar y destruir su cuenta.
- cememberable: Maneja el registro de usuarios a través de un proceso de registro, lo que también les permite editar y destruir su cuenta.
- trackable: Rastrea el recuento de inicios de sesión, las marcas de tiempo y la dirección IP.
- timeoutable: Expira las sesiones que no han estado activas en un período de tiempo específico.
- validatable: Proporciona validaciones de correo electrónico y contraseña. Es opcional y se puede personalizar, por lo que puede definir sus propias validaciones.
- lockable: Locks an account after a specified number of failed sign-in attempts. Can unlock via em

El mismo resultado del comando que compartió Wusinho se puede lograr con: `rails routes -g user`

Añadido a eso, también podemos hacer:

- `rails routes -g GET` (para obtener las rutas que tengan el método *GET*)
- `rails routes -g show` (filtrando por el método/acción *show*)

Pueden encontrar más información al respecto en la [documentación de las guías de Rails](https://guides.rubyonrails.org/routing.html#listing-existing-routes).

[![img](https://www.google.com/s2/favicons?domain=https://rubygems.org/gems/devise/versions/4.2.0/apple-touch-icons/apple-touch-icon.png)devise | RubyGems.org | your community gem host](https://rubygems.org/gems/devise/versions/4.2.0)

https://github.com/imhta/rails_6_devise_example

## Asignando un propietario a la tarea

Crear una migration con referencia manual

```shell
❯ rails g migration AddOwnerToTask user:references
```

error permision, resetear la base de datos. Solo, si esta no esta siendo utilizado en production.

```sh
❯ rails db:reset
```

luego correr la migration

```
rails db:migration
```

migration

```sh
❯ rails db:migrate
== 20220630005833 AddOwnerToTask: migrating ===========
========================
-- add_reference(:tasks, :owner, {:null=>false, :foreig
n_key=>{:to_table=>:users}, :index=>true})
   -> 0.2503s
== 20220630005833 AddOwnerToTask: migrated (0.2504s) ==
========================
```

El objetivo de esta clase es hacer una migración manual para enlazar la llave foránea de la tarea junto con la asignación del usuario.

------

Para comenzar hay que generar una migración con el comando `rails g migration`, este comando es útil porque los modelos se siguen actualizando, así que cada vez que se quiera hacer una actualización o modificar una estructura de la base de datos hay que hacer una migración.

Esta migración tiene el objetivo de añadir un propietario a una tarea por lo que le daremos el nombre AddOwnerToTask y le añadiremos la referencia del usuario con user:references.

El comando completo sería: `rails g migration AddOwnerToTask user:references`. Esto generará una nueva migración en la carpeta db>migrate.

En el archivo de migración se encuentra el método add_reference, que recibe la tabla a la cuál se va a relacionar, el nombre de la relación que se usará internamente y la llave foránea, el tercer parámetro indica que la relación no puede ser nula y el cuarto señala que se trata de una llave foránea.

```ruby
add_reference :tasks, :user, null: false, foreign_key: true
```

Ahora, user no sería el mejor nombre para la relación entre la task y user, sería mejor llamarla owner, pero al cambiar el nombre de la relación también hay que indicarle a la llave foránea la tabla en la que debe buscar, ya que no existe ninguna tabla llamada owner. Además se la agregará un index para que tenga un mejor rendimiento.

```ruby
add_reference :tasks, :owner, null: false, 
foreign_key: { to_table: :users}, index: true
```

Lo único que falta es añadir el campo al model de Task con el método belongs_to :owner, pero, como no existe el modelo owner también hay que indicarle explícitamente que se trata del model User.

```ruby
belongs_to :owner, class_name: 'User'
```

Lo siguiente es ir al model de User y añadir la propiedad has_many :tasks.

Finalmente hay que ejecutar el comando `rails db:migrate`. Si existían registros de usuarios tendrán que ser eliminados para que se pueda ejecutar la migración. Se puede ejecutar el comando `rails db:reset` volver a la versión inicial de la base de datos, también se ejecutarán las migraciones de la carpeta db>migrate. Para hacer el reset hay que parar la ejecución del servidor.

Rails es una tecnología que está pensada naturalmente para tres ambientes (sin que esto signifique no puedas tener más…): development, test, production.

por defecto en Rails trabajamos en el ambiente de **development** y para correr las tareas pre-construidas de rails en un ambiente distinto podemos usar **RAILS_ENV** asi `RAILS_ENV=test rails db:migrate` si estamos usando un gestor de versiones avanzado como **rbenv** (binstubs), o `RAILS_ENV=test bundle exec rake db:migrate` si no estamos usando binstubs

## Añadiendo participantes a la tarea

Al correr la migracion y recargar el servidor.

Nuestra un errror

Copiar y pegar `app/models/user.rb` 

```ruby
has_many :owned_tasks, class_name: 'Task'
```

corrige el error.

`tasks_controller.rb`

```ruby
def create
  ...
  @task.owner = current_user
  ...
end
```

Validacion de usuario registrado

### Generar model Participant

```sh
❯ rails g model Participant role:integer user:reference
s task:references
```

Validaciones en `model/task.rb`

```ruby
  has_many :participating_users, class_name: 'Participant'
  has_many :Participants, through: :participating_users, source: :user

  validates :participating_users, presence: true
```

Validaciones en `model/user.rb`

```ruby
  has_many :owned_tasks, class_name: 'Task'
  has_namy :participations, class_name: 'Participant'
  has_namy :tasks, throught: :participations
```

## Creando formularios anidados

gemfile

```ruby
gem "cocoon"
```

instalar 

```sh
bundle install
```

Instalar dependencias con `yarn`

```sh
❯ yarn add 'github:nathanvda/cocoon#c24ba53'
```

Para instalar esta gema hay que agregarla en el Gemfile, lo siguiente es agregar una dependencia de Cocoon para que pueda trabajar de forma  correcta, esta dependencia se agrega con yarn desde el siguiente  repositorio: github:nathanvda/cocoon#c24ba53.

#### Configuración de cocoon

En el archivo eviroment.js:

```ruby
const webpack = require('webpack')

environment.plugins.prepend('Provide',
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        Popper: ['popper.js', 'default'],
    })
)
```

En el archivo application.js:

```ruby
import 'cocoon'
```

Esta configuración es necesaria ya que cocoon tiene como dependencia a jquery.

Una vez hecho esto hacemos la migración, para migrar el modelo Participant (`rails db:migrate`).

------

#### Formulario de tasks

En el formulario de tasks es donde se requiere asociar con los  participantes, para esto se usará simple_fields_for, un método que  permite asociar un modelo anidado que exista en la estructura de  modelos.

```ruby
= simple_form_for(@task) do |f|
  = f.error_notification
  = f.error_notification message: f.object.errors[:base].to_sentence if f.object.errors[:base].present?

  .form-inputs
    = f.input :name, label: t('tasks.name')
    = f.input :description, label: t('tasks.description')
    = f.input :due_date, label: t('tasks.due_date')
    = f.association :category, label: t('tasks.category')
    
    .participants
      = f.simple_fields_for :paticipating_users
```

Con el método simple_fields_for se habilita la comunicación con  participating_users, que es como se nombró la relación entre tareas y  participantes.

Para el formulario principal el constructor es f (primera línea) y para el formulario anidado el constructor es g.

Para presentar el formulario anidado se usará un parcial, que es un  archivo creado en la carpeta views>tasks con el nombre  _participating_user_fields.html.haml. Y se llamará al formulario  principal con la función render dentro del formulario anidado.

```ruby
.participants
      = f.simple_fields_for :paticipating_users
        = render 'participating_users_fields', f: g
```

Parcial:

```ruby
.nested-fields 
    = f.input :user_id, as: :select, collections: User.all - [current_user]
    = f.input :role
```

En el primer input se inserta un elemento select, para poder elegir  entre todos los usuarios, pero le restamos el usuario actual para que no aparezca en la lista. Era necesario poner current_user en un arreglo  para que Ruby pudiera realizar las operaciones entre arreglos con  User.all (también es un arreglo) sin problemas.

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - nathanvda/cocoon: Dynamic nested forms using jQuery made easy; works with formtastic, simple_form or default forms](https://github.com/nathanvda/cocoon#c24ba53)

[![img](https://www.google.com/s2/favicons?domain=https://rubygems.org/gems/cocoon/apple-touch-icons/apple-touch-icon.png)cocoon | RubyGems.org | your community gem hosthttps://rubygems.org/gems/cocoon](https://rubygems.org/gems/cocoon)

## Interactuando con Cocoon para anidar formularios

Agregar participantes `views/tasks/_form.html.haml`

```ruby
 #addParticipants 
        = link_to_add_association f, = :participating_users, 'data-association-insertion-node' => '.participants .participants .participants-container', 'data-turbolinks' => false do
          agregar participante
      .participants
        = f.simple_fields_for :participating_users do |g|
          = render 'participating_user_fields', f: g
        .participants-container
```



## i18n-tasks | RubyGems.org | your community gem hostCanCan: ¿puedes hacerlo?

# 6. Proyecto del curso: interacciones

## Callbacks en Rails

## Añadiendo datos semilla

## Enviando e-mails a los participantes

## Añandiendo notas a la tarea

## Añadiendo notas con AJAX

# 7. Cierre

## Embelleciendo nuestra aplicación

## Desplegando a Heroku

## Conclusiones del curso
